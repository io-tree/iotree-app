import React, { useEffect } from 'react';
import Routes from './src/routes';
import FlashMessage from "react-native-flash-message";
import { StatusBar } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


import colors from './src/globalStyles/colors';
import { MenuProvider } from 'react-native-popup-menu';
import api from './src/services/api';

export default function App() {

  	return (
		<>
			<StatusBar backgroundColor={colors.primaryDark}/>
			<MenuProvider>
				<Routes />
			</MenuProvider>
			<FlashMessage 
				position="bottom" 
				autoHide={true}
				duration={5000}
			/>
		</>
	)
}
