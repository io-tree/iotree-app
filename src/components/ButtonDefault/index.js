import React from 'react';
import {
	TouchableOpacity,
	Text
} from 'react-native';

import styles from './styles';

export default ButtonDefault = ({onSubmit, buttonLabel, disabled=false, buttonType=1, textType=1}) => {
	return (
		<TouchableOpacity onPress={onSubmit} style={styles['button' + buttonType]} disabled={disabled}>
			<Text style={styles['buttonText'+textType]}>{buttonLabel}</Text>
		</TouchableOpacity>
	)
}