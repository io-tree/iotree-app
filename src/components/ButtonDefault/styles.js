import colors from '../../globalStyles/colors'
import fonts from '../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({

  	button1: {
		alignItems: 'center',
		height: height/15,
		borderRadius: 15,
		marginTop: height/15,
		backgroundColor: colors.primaryDark,
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: width/10,
		elevation:3,
		marginBottom:height/20
	},

	button2: {
		alignItems: 'center',
		borderRadius: 8,
		padding: 10,
		backgroundColor: colors.primaryDark,
		elevation:3,
		alignSelf: 'center',
		marginVertical: 10
	},

  	buttonText1: {
		fontFamily:fonts.medium,
		fontSize: fonts.l,
		color: colors.textColor,
	},

	buttonText2: {
		fontFamily: fonts.medium,
		fontSize: fonts.s,
		color: colors.textColor,
	}

});
export default styles;