import colors from '../../globalStyles/colors'
import fonts from '../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  	container: {
		flex: 1,
		justifyContent: 'center'
	},

	img: {
		height: height, 
		width: width, 
		resizeMode: 'contain'
	}
});
export default styles;