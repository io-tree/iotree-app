import React, { useEffect, useState } from 'react';
import {
	ImageBackground,
	Image,
	Animated
} from 'react-native'

import styles from './styles';



export default function LazyImage({sourceImg, smallSourceImg, shouldLoad}) {

	const [load, setLoad] = useState(false);
	const opacity = new Animated.Value(0);

	const OriImg = Animated.createAnimatedComponent(Image)

	useEffect( () => {
		if(shouldLoad) {
			setTimeout(()=> {
				setLoad(true)
			}, 100)
		}
	}, [shouldLoad])

	function handleAnimate() {
		Animated.timing(opacity, {
			toValue: 1,
			duration: 500,
			useNativeDriver: true
		}).start()
	}

	return (
		<ImageBackground source={{uri:smallSourceImg}} style={styles.img} blurRadius={0.5}>
			{load && 
				<OriImg 
					source={{uri: sourceImg}} 
					style={{...styles.img, opacity}}
					onLoadEnd={handleAnimate}	
				/>
			}
		</ImageBackground>
	)
}

