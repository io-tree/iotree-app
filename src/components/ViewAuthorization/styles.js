import { 
	StyleSheet, 
} from 'react-native';
import colors from '../../globalStyles/colors';
import fonts from '../../globalStyles/fonts';

export default StyleSheet.create({
	container: {
		flex: 1,
		alignSelf: 'stretch',
		paddingHorizontal: 25,
		paddingTop: 20
	},

	containerPreview: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},

	containerPreviewMessage: {
		
	}
}) 