import React, { useEffect, useState} from 'react';
import {
	View,
	Text,
	TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';

import styles from './styles';

export default ({children, navigation, headerRight=false}) => {

	const [ pass, setPass ] = useState(false);
	const [ loading, setLoading ] = useState(false);

	let didWillFocus = null;

	useEffect(() => {

		(async () => await init())();

		didWillFocus = navigation.addListener('willFocus', async () => {
			await init();
		});
	}, []);

	async function init() {
		setLoading(true);
		const data = await AsyncStorage.getItem('user');
		setPass((data ? true: false))
		setLoading(false);
	}


	useEffect(() => () => {
		didWillFocus.remove();
	}, []);


	useEffect(() => {
		if(headerRight && pass){
			navigation.setParams({headerRight: ({ tintColor }) => (
				<TouchableOpacity onPress={()=> headerRight.function()}>
					<FontAwesomeIcon 
						name={headerRight.iconName}  
						color={(headerRight.iconColor ? headerRight.iconColor : tintColor)} 
						size={(headerRight.iconSize ? headerRight.iconSize : 25)} 
					/> 
				</TouchableOpacity>
			)});
		} else {
			navigation.setParams({headerRight: ()=> null});
		}
	}, [pass])

	return (
		<View style={styles.container}>
			{pass ? 
				children
			: 
				!loading && (
					<TouchableOpacity style={styles.containerPreview} onPress={() => navigation.navigate('Login')}>
						<Text style={styles.containerPreviewMessage}>Você precisa estar logado :(</Text>
					</TouchableOpacity>
				)
			}
		</View>
	)
}