import React from 'react';
import {
	View,
	Text,
	Modal
} from 'react-native';
import LottieView from 'lottie-react-native';

import { loads, loadSelected } from '../../services/jsonLoading';

export default function LoadingScreen({enabled, textLoading = null}) {
	return (
		<Modal
			transparent={true}
			visible={enabled}
		>
			<View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.8)'}}>
				<LottieView
					source={loads[loadSelected][0]}
					autoPlay
					speed={loads[loadSelected][1]}
					loop
					style={{
						width: loads[loadSelected][2]
					}}
				/>
				{textLoading &&
					<Text style={{
						color: 'white',
						position: 'absolute',
						bottom: "15%",
						fontSize: 18
					}}>{textLoading}</Text>
				}
			</View>
		</Modal>
	)
}