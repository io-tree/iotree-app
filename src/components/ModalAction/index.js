import React, { useState } from 'react';
import {
	Modal,
	View,
	TouchableOpacity,
	Text
} from 'react-native';

import styles from './styles';

export default ({children ,enabled, listAction, onClose}) => {

	return (
		<Modal
			transparent={true}
			visible={enabled}
		>
			<View style={styles.container}>
				<View style={styles.modalContainer}>
					<View style={styles.modalInfo}>
						{children}
					</View>
					<View style={styles.modalButtonContainer}>
						{listAction.map( (action, index) => {
							return (
								<TouchableOpacity key={index} style={styles.modalButton} onPress={() => {
									onClose()
									if(action.function)
										action.function()
								}}>
									<Text style={styles.modalButtonText}>{action.label}</Text>	
								</TouchableOpacity>
							)
						})}
					</View>
				</View>
			</View>
		</Modal>
	)
}