import colors from '../../globalStyles/colors'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({

	container: {
		flex: 1, 
		justifyContent: 'center', 
		alignItems: 'center', 
		backgroundColor: 'rgba(0,0,0,0.7)'
	},

	modalContainer: {
		backgroundColor: 'white',
		width: '70%',
		borderRadius: 0
	},

	modalInfo: {
		padding: 10
	},

	modalButtonContainer: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: "space-around"
	},

	modalButton: {
		height: 30,
		backgroundColor: colors.primaryDark,
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},

	modalButtonText: {
		color: 'white'
	}
});
export default styles;