import React, { useCallback, useState } from 'react';
import {
	ScrollView,
	RefreshControl
} from 'react-native';

import colors from '../../globalStyles/colors';

import loadingScreen from '../LoadingScreen';
import LoadingScreen from '../LoadingScreen';

export default ({children, onRefresh, loadingScreen=true}) => {

	const [ refreshing, setRefreshing ] = useState(false);

	const refreshFunction = useCallback(() => {
		onRefresh();
	}, [])

	return (
		<ScrollView
			refreshControl={
				<RefreshControl colors={['white']} progressBackgroundColor={colors.primary} refreshing={refreshing} onRefresh={refreshFunction}/>
			}
		>
			<LoadingScreen enabled={refreshing} />
			{children}
		</ScrollView>
	)
}
