import React, {useState, useEffect} from 'react';
import {
	View,
	Text,
	TouchableOpacity
} from 'react-native';
import RBSheet from "react-native-raw-bottom-sheet";
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';

import styles from './styles';

export default ({listAction, enabled, onClose}) => {

	const height = 100 * Math.ceil(listAction.length / 5);
	const [menu, setMenu] = useState(null);

	useEffect(() => {
		if(menu)
			if(enabled)
				menu.open();
			else
				menu.close();
	}, [enabled])
	
	return (
		<RBSheet
			ref={ref => { 
				return setMenu(ref);
			}}
			onClose={onClose}
			duration={250}
			height={height}
			customStyles={{container: styles.menuSheetContainer}}
		>
			{listAction.map( (action, index) => {
				return (
					<MenuSheet key={index} iconName={action.icon} iconText={action.label} func={action.function} />
				)
			})}
		</RBSheet>
	)

	function MenuSheet({iconName, iconText, func}){
		return (
			<TouchableOpacity style={styles.menuSheet} onPress={() => {
				menu.close();
				func()
			}}>
				<View style={styles.menuSheetIconContainer}>
					<FontAwesomeIcon name={iconName} color={'white'} size={15} />
				</View>
				<Text style={styles.menuSheetText}>{iconText}</Text>
			</TouchableOpacity>
		)
	}
}

