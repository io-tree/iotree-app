import colors from '../../globalStyles/colors'

import { StyleSheet } from 'react-native'

let menuSheetIconSize = 40;

const styles = StyleSheet.create({
	menuSheetContainer: {
		justifyContent: "space-evenly",
		alignItems: "flex-start",
		flexWrap: 'wrap',
		flexDirection: "row",
		paddingVertical: 15
	},

	menuSheet: {
		alignItems: 'center',
		marginBottom: 10,
		width: 80,
		height: 'auto',
		justifyContent: 'flex-start',
	},

	menuSheetIconContainer: {
		backgroundColor: colors.primaryDark,
		alignItems: "center",
		justifyContent: 'center',
		width: menuSheetIconSize,
		height: menuSheetIconSize,
		borderRadius: menuSheetIconSize
	},

	menuSheetText: {
		fontSize: 12,
		marginTop: 3,
		textAlign: 'center'
	},

});
export default styles;