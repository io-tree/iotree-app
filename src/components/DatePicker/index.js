import React, { useState } from 'react';
import { 
	Text,
	TouchableOpacity
} from 'react-native';

import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';

import styles from './styles';

export default DatePicker = ({ labelText, setDate, date = null, labelSelect = 'Selecione uma data', type=1}) => {

	const [dateCalender, setDateCalender] = useState(date);
	const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

	let styleNameButton = 'dateButton' + type;
	let styleNameTitle = 'dateTitle' + type;
	let styleNameText = 'dateText' + type;

	const handleConfirm = date => {
		setDatePickerVisibility(false);
		setDateCalender(date);
		setDate(moment(date).format('YYYY-MM-DD'));
	};

	return(
		<>
			{isDatePickerVisible && (
				<DateTimePickerModal
					date={(dateCalender === null ? new Date() : dateCalender)}
					isVisible={isDatePickerVisible}
					mode="date"
					onConfirm={handleConfirm}
					onCancel={() => setDatePickerVisibility(false)}
				/>

			)}
			<TouchableOpacity style={styles[styleNameButton]} onPress={() => setDatePickerVisibility(true)} >
				{ type == 1 &&
					<Text style={styles[styleNameTitle]}>{labelText}</Text>
				}
				<Text style={styles[styleNameText]}>{dateCalender ? moment(dateCalender).format('DD/MM/YYYY') : labelSelect}</Text>
			</TouchableOpacity>
		</>
	)
}