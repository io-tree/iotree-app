import colors from '../../globalStyles/colors'
import fonts from '../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'

export default StyleSheet.create({
	
	dateButton1: {
		marginTop:30,
		borderBottomWidth:1,
		paddingBottom:10,
		borderColor:colors.gray,
	},
	dateTitle1:{
		fontFamily:fonts.medium,
		fontSize:fonts.xs,
		color:colors.grayDark
	},
	dateText1:{
		fontFamily:fonts.medium,
		fontSize:fonts.m,
		color:colors.black
	}, 	

	dateButton2: {
		flex: 1,
		paddingVertical: 5,
		marginLeft: 10,
	},
	dateTitle2:{
		fontFamily:fonts.medium,
		fontSize:fonts.xs,
		color:colors.grayDark
	},
	dateText2:{
		fontFamily:fonts.regular,
		fontSize:fonts.m,
		color:colors.black,
		marginLeft: 5,
		textAlignVertical: 'center'
	}, 	
});