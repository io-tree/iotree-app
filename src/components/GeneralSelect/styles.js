import colors from '../../globalStyles/colors'
import fonts from '../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

export default StyleSheet.create({

	pickerContainer:{
		borderWidth:1,
		borderColor:colors.gray,
		flex: 1,
	},
	
	pickerContainer2: {
		flex: 1,
		marginLeft: 5,
		fontFamily:fonts.regular,
		fontSize:fonts.m,
	},
	
	picker: {
		fontFamily:fonts.regular,
		fontSize:fonts.m,
		backgroundColor: '#000'
	}
});