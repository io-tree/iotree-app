import React, { useEffect, useState } from 'react';
import { 
	View, 
	Picker
} from 'react-native'

import api from '../../services/api';

import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';

export default GenderSelect = ({ idSelected, setIdSelected, model, type = 1 }) => {

	const [ load, setLoad ] = useState(true);
	const [ generalList, setGeneralList ] = useState([]);

	let urlModel = '';
	let msgModel = '';

	useEffect(() => {
		(async () => {
			let id_user = await AsyncStorage.getItem('user');
			if(id_user) id_user = (JSON.parse(id_user)).id_user;

			switch(model){
				case 'gender': {
					urlModel = '/genders'
					msgModel = 'Selecione seu gênero'
					break;
				}
				case 'greenhouse': {
					urlModel = `/users/${id_user}/greenhouses`
					msgModel = 'Selecione sua estufa'
					break;
				}
			}

			const response = await api.get(urlModel);

			switch(model){
				case 'greenhouse': {
					setGeneralList([
						{
							code: 0,
							name: msgModel
						},
						...response.data.body.filter( item => !item.plantation)
					])
					break;
				}
				case 'gender': {
					setGeneralList([
						{
							id_gender: 0,
							name: msgModel
						},
						...response.data.body
					])
					break;
				}
			}
			setLoad(false)
		})();
	}, [])

	return (
		<View style={styles['pickerContainer'+type]}>
			<Picker
				selectedValue={idSelected}
				itemStyle={styles.picker}
				mode={'dropdown'}
				onValueChange={(itemValue, itemIndex) =>
					setIdSelected(itemValue)
				}
			>
				{!load && generalList.map((gr, index) => {
					switch(model){
						case 'gender':
							return (<Picker.Item key={index} label={gr.name} value={gr.id_gender} />)
						case 'greenhouse':
							return (<Picker.Item key={index} label={gr.name} value={gr.code} />)
					}
				})}
			</Picker>
		</View>
	)
}