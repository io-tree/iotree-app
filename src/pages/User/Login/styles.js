import colors from '../../../globalStyles/colors'
import fonts from '../../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  	container: {
		flex: 1
	},

  	textFieldsContainer:{
		marginTop:height/10,
		paddingHorizontal:width/10
	},

  	textField: {
		marginTop: height/35,
		flex: 1,
	},
	
	textFieldText:{
		fontFamily:fonts.medium,
		color:colors.black
	},

	textFieldLabel: {
		paddingHorizontal: 20
	},

	iconToggleVisible: {
		position: "absolute", 
		alignItems: "center",
		justifyContent: "center",
		right: 0, 
		top: 20,
		width: 40,
		height: 40,
		borderRadius: 20,
	},
  
	buttonContainer:{
		alignItems: 'center'
	},

  	button: {
		alignItems: 'center',
		height: height/15,
		borderRadius: 15,
		marginTop: height/15,
		backgroundColor: colors.primaryDark,
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: width/10,
		borderWidth:1,
		borderColor:colors.black,
		elevation:3,
		marginBottom:height/20
	},

  	buttonText: {
		fontFamily:fonts.medium,
		fontSize: fonts.l,
		color: colors.white,
	},

 	footer: {
		marginTop:height/15,
		paddingHorizontal:height/30,
		flex: 1,
		justifyContent: 'center',
		alignSelf: 'center',
	},

  	footerText: {
		fontFamily:fonts.medium,
		textAlign: 'center',
		color: colors.grayDark,
		fontSize: fonts.m,
		marginBottom: height/80
	},

	profileImage: {
		marginTop: 10,
		width: 150,
		height: 150,
		marginBottom: 20,
		borderRadius: 75,
		alignSelf: "center"
	},

	profile: {
		paddingHorizontal:width/8,
		flexDirection: 'row',
		alignSelf: 'flex-start',
	},

	profileLabel: {
		fontFamily: fonts.regular,
		fontSize: fonts.xl,
		color:colors.black,
		marginTop:height/80,
		textAlignVertical:'center'
	},
	profileSeparator: {
		color: colors.primaryLight,
		fontSize: fonts.xl,
		marginTop:height/80,
		textAlignVertical:'center',
		marginLeft:width/80
	},
	profileText: {
		fontFamily:fonts.regular,
		fontSize:fonts.m,
		color:colors.black,
		marginTop:height/80,
		textAlignVertical:'center',
		marginLeft:width/80,
		marginRight:width/10
	},

	modalContainer: {
		backgroundColor: 'white',
		width: '70%',
		borderRadius: 0
	},

	modalInfo: {
		padding: 10
	},

	modalButtonContainer: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: "space-around"
	},

	modalButton: {
		height: 30,
		backgroundColor: colors.primaryDark,
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},

	modalButtonText: {
		color: 'white'
	}
});
export default styles;