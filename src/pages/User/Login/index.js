import React, { useState, useEffect, useCallback } from 'react';
import {
  View,
  Text,
  RefreshControl,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  Modal
} from 'react-native';
import { showMessage } from "react-native-flash-message";
import { TextField } from 'react-native-material-textfield';
import moment from 'moment';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5'

import api from '../../../services/api';
import socket from '../../../services/socket';
import colors from '../../../globalStyles/colors';
import styles from './styles';
import exceptions from '../../../services/exceptions';
import AsyncStorage from '@react-native-community/async-storage';

import LoadingScreen from '../../../components/LoadingScreen';
import ButtonDefault from '../../../components/ButtonDefault';
import ScrollViewRefresh from '../../../components/ScrollViewRefresh';
import ModalAction from '../../../components/ModalAction';

export default function Login ({ navigation }) {

  let didWillFocus = null;

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [login, setLogin] = useState(false);
  const [user, setUser] = useState(null);
  const [load, setLoad] = useState(false);
  const [checkEmailModal, setCheckEmailModal] = useState(false);
  const [isVisibled, setIsVisible] = useState(false);


  useEffect(() => {
    getUser();
    didWillFocus = navigation.addListener('willFocus', getUser);
  }, []);

  useEffect(() => () => {
    didWillFocus.remove();
  }, [])

  const getUser = useCallback(async () => {
    let id_user = await AsyncStorage.getItem('user');
    if (id_user) {
      id_user = (JSON.parse(id_user)).id_user;

      let user = await api.get(`/users/${id_user}`);
      if (user) {
        user = user.data.body;
        let is_done = 0,
          not_done = 0;
        user.plantations.map(plantation => {
          if (plantation.is_done) {
            is_done++;
          } else {
            not_done++;
          }
          return {
            is_done,
            not_done
          }
        })

        user.plantations = {
          is_done,
          not_done
        }

        await AsyncStorage.setItem('user', JSON.stringify(user))
        setUser(user);
        setLogin(true);
        socket.emit('user_connect', user);
      } else {
        setLogin(false)
      }
    }
  }, []);

  function reSendEmail () {
    api.post('/verification/' + email).then(res => {
      showMessage({
        type: (res.data.status === 'ok' ? 'info' : 'danger'),
        message: res.data.msg
      })
    }).catch(err => {
      console.log(err)
    })
  }

  function onSubmit () {
    setLoad(true)
    api.post('/sessions', {
      email,
      password,
      stayConnected: true
    }
    ).then(async user => {
      if (user.data.status === 'ok') {
        await AsyncStorage.setItem('user', JSON.stringify(user.data.body));
        await AsyncStorage.setItem('keyConnection', user.data.keyConnection)
        await getUser()
        setLogin(true);
        socket.emit('user_connect', user.data.body);
      } else {
        if (user.data.code === 'userVerifyEmail') {
          setCheckEmailModal(true);
        } else {
          let temp = '';
          if (user.data.body) {
            user.data.body.map(err => {
              temp += err + '\n';
            })

          } else {
            temp = user.data.msg;
          }
          showMessage({
            message: temp,
            type: "danger",
          });
        }
      }
      setLoad(false);
    }).catch(err => {
      showMessage(exceptions(err))
      setLoad(false);
    })

  }

  async function userDisconnect () {
    await AsyncStorage.clear();
    setUser(null);
    setLogin(false);
    socket.emit('user_disconnect');
  }

  const loginScreen = (
    <View style={styles.container}>
      <CheckEmailModal enabled={checkEmailModal} setEnabled={setCheckEmailModal} sendEmail={reSendEmail} />
      <LoadingScreen enabled={load} textLoading={'Entrando na sua conta'} />
      <ScrollView>
        <View style={styles.textFieldsContainer}>
          <View style={styles.textField}>
            <TextField
              label='Email'
              title='Digite seu email'
              tintColor={colors.primary}
              keyboardType="email-address"
              autoCapitalize="none"
              autoCorrect={false}
              autoCompleteType="off"
              value={email}
              nextFocusDown={true}
              onChangeText={setEmail}
              style={styles.textFieldText}
              titleTextStyle={styles.textFieldText}
              labelTextStyle={styles.textFieldText}
            />
          </View>
          <View style={styles.textField}>
            <TextField
              label='Senha'
              title='Digite sua senha'
              tintColor={colors.primary}
              autoCapitalize="none"
              secureTextEntry={!isVisibled}
              autoCorrect={false}
              autoCompleteType="password"
              value={password}
              onChangeText={setPassword}
              style={styles.textFieldText}
              titleTextStyle={styles.textFieldText}
              labelTextStyle={styles.textFieldText}
              inputContainerStyle={{ paddingRight: 35 }}
            />
            <TouchableHighlight
              onPress={() => setIsVisible(!isVisibled)}
              style={styles.iconToggleVisible}
              underlayColor={colors.primaryLight}
              activeOpacity={0}
            >
              <FontAwesomeIcon name={isVisibled ? "eye" : "eye-slash"} color={colors.primaryDark} size={20} />
            </TouchableHighlight>
          </View>
        </View>

        <View style={styles.buttonContainer}>
          <ButtonDefault buttonLabel={'Entrar'} onSubmit={onSubmit} />
        </View>

        <View style={styles.footer}>
          <Text onPress={() => navigation.navigate('RecoverPassword')} style={styles.footerText}>
            Esqueceu sua senha? pressione aqui
					</Text>
          <Text onPress={() => navigation.navigate('Create')} style={styles.footerText}>
            Clique aqui para se cadastrar
					</Text>
          <Text style={styles.footerText}>
            Não tem um produto, clique aqui para saber mais
					</Text>
        </View>
      </ScrollView>
    </View>
  )

  const logoutScreen = (user && (
    <View style={styles.container}>
      <ScrollViewRefresh
        onRefresh={
          () => getUser()
        }
      >
        <Image
          style={styles.profileImage}
          source={{
            uri: api.defaults.baseURL + user.path_img + '?random_number=' + new Date().getTime(),
          }}

        />
        <ProfileLabel label={'Nome'} text={user.name} />
        <ProfileLabel label={'Idade'} text={moment().diff(new Date(user.birthday_date), 'year')} />
        <ProfileLabel label={'Plantas cultivadas'} text={user.plantations.is_done} />
        <ProfileLabel label={'Plantas em cultivo'} text={user.plantations.not_done} />
        <ProfileLabel label={'Email'} text={user.email} />
        <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-evenly' }}>
          <ButtonDefault buttonLabel={'Editar'} onSubmit={() => navigation.navigate('Edit', { id: user.id_user })} />
          <ButtonDefault buttonLabel={'Sair'} onSubmit={userDisconnect} />
        </View>
      </ScrollViewRefresh>
    </View>
  ))


  return (
    <>
      {!login ? loginScreen : logoutScreen}
    </>
  );
}

function ProfileLabel ({ label, text }) {
  return (
    <View style={styles.profile}>
      <Text style={styles.profileLabel}>{label}</Text>
      <Text style={styles.profileSeparator}> | </Text>
      <Text style={styles.profileText}>{text}</Text>
    </View>
  )
}

function CheckEmailModal ({ enabled, setEnabled, sendEmail }) {
  return (
    <ModalAction
      enabled={enabled}
      onClose={() => setEnabled(false)}
      listAction={[
        {
          label: 'Ok'
        },
        {
          label: 'Reenviar email',
          function: () => sendEmail()
        }
      ]}
    >
      <Text>Verifique seu email na sua caixa de mensagens</Text>
      <Text>Ou reenvie o email</Text>
    </ModalAction>
  )
}