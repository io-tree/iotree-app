import React, { useState, useEffect } from 'react';
import { 
	View, 
	Text,
	ScrollView,
	TouchableOpacity, 
} from 'react-native'
import LoadingScreen from '../../../components/LoadingScreen';
import GenderSelect from '../../../components/GeneralSelect';
import DatePicker from '../../../components/DatePicker';
import { showMessage } from "react-native-flash-message";
import styles from './styles'
import colors from '../../../globalStyles/colors';
import { TextField } from 'react-native-material-textfield';

import api from '../../../services/api';
import exceptions from '../../../services/exceptions';
export default function CreateUser({ navigation }) {
	const [load, setLoad] = useState(false);

	const [name, setName] = useState(null);
	const [birthday_date, setBirthday_date] = useState(null);
	const [email, setEmail] = useState(null);
	const [password, setPassword] = useState(null);
	const [id_gender, setId_gender] = useState(null);

	function submitData(){
		setLoad(true)
		console.log('2',birthday_date)
		api.post('/users', {
				name,
				birthday_date,
				email,
				password,
				id_gender
			}
		).then( user => {
			if(user.data.status === 'ok'){
				showMessage({
					message: user.data.msg,
					type: "success",
					description: 'Um email foi enviado, verifique para acessar sua conta'
				});
				navigation.goBack()
			}else{
				/* let temp = '';
				if(user.data.body){
					user.data.body.map( err => {
						temp += err[global.language] + '\n';
					})
	
				} else {
					temp = user.data.msg;
				} */
				showMessage({
					message: user.data.msg,
					type: "danger",
				});
			}
			setLoad(false)
		}).catch( err => {
			console.log(err)
			showMessage(exceptions(err))
			setLoad(false);
		});
	}

	return (
		<View style={styles.container}>
			<LoadingScreen enabled={load}/>
			<ScrollView>
				<View style={styles.textFieldsContainer}>
					<View style={styles.textField}>
						<TextField
							label='Nome'
							title='Digite seu nome'
							tintColor={colors.primary}
							autoCapitalize="words"
							autoCorrect={true}
							autoCompleteType="name"
							value={name}
							onChangeText={setName}
							style={styles.textFieldText}
							titleTextStyle={styles.textFieldText}
							labelTextStyle={styles.textFieldText}
						/>
					</View>
					<View style={styles.textField}>
						<TextField
							label='Email'
							title='Digite seu email'
							tintColor={colors.primary}
							keyboardType='email-address'
							autoCapitalize="none"
							autoCompleteType="email"
							value={email}
							onChangeText={setEmail}
							style={styles.textFieldText}
							titleTextStyle={styles.textFieldText}
							labelTextStyle={styles.textFieldText}
						/>
					</View>
					<View style={styles.textField}>
						<TextField
							label='Senha'
							title='Digite sua senha'
							tintColor={colors.primary}
							autoCapitalize="none"
							secureTextEntry={true}
							autoCorrect={false}
							autoCompleteType="password"
							value={password}
							onChangeText={setPassword}
							style={styles.textFieldText}
							titleTextStyle={styles.textFieldText}
							labelTextStyle={styles.textFieldText}
						/>
					</View>

					<GenderSelect 
						idSelected={id_gender}
						setIdSelected={setId_gender}
						model='gender'
					/>

					<DatePicker 
						labelText={'Data de Nascimento'}
						setDate={setBirthday_date}
						labelSelect={'Selecione sua data de nascimento'}
					/>


					<TouchableOpacity style={styles.cadastrarButton} onPress={submitData}>
						<Text style={styles.cadastrarText}>
							Cadastrar
						</Text>
					</TouchableOpacity>
				</View>
			</ScrollView>
		</View>
	)
}