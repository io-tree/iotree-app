import colors from '../../../globalStyles/colors'
import fonts from '../../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

export default StyleSheet.create({
	container: {
		flex: 1
	},

	textFieldsContainer:{
		marginHorizontal: 30
	},
	textField: {
		marginVertical: 2.5,
	},
	textFieldText:{
		fontFamily:fonts.medium,
		color:colors.black
	},
	pickerContainer:{
		marginTop:30,
		borderWidth:1,
		borderColor:colors.gray
	},
	dataNascimentoButton: {
		marginTop:30,
		borderBottomWidth:1,
		paddingBottom:10,
		borderColor:colors.gray,
	},
	dataNascimentoTitle:{
		fontFamily:fonts.medium,
		fontSize:fonts.xs,
		color:colors.grayDark
	},
	dataNascimentoText:{
		fontFamily:fonts.medium,
		fontSize:fonts.m,
		color:colors.black
	},
	cadastrarButton:{
		alignItems: 'center',
		height: height/15,
		borderRadius: 15,
		marginTop: height/15,
		backgroundColor: colors.primaryDark,
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: width/15,
		borderWidth:1,
		borderColor:colors.black,
		elevation:3
	},
	cadastrarText: {
		fontFamily:fonts.medium,
		fontSize: fonts.l,
		color: colors.white,
	},
	  
	
});