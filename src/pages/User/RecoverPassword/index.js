import React, { useState, useEffect } from 'react';
import {
	View,
	Text
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import { TextField } from 'react-native-material-textfield';

import api from '../../../services/api';
import colors from '../../../globalStyles/colors';
import styles from './styles';
import LoadingScreen from '../../../components/LoadingScreen';
import ButtonDefault from '../../../components/ButtonDefault';
import { showMessage } from 'react-native-flash-message';

export default function RecoveryPassword({navigation}) {

	const [ email, setEmail ]= useState(undefined);

	const [ load, setLoad ] = useState(true);

	useEffect( () => {
		setLoad(false);
	}, []);

	async function setSavePassword() {
		setLoad(true); 


		let user = await api.post('/recoveryPassword',{
			email
		});

		if(user.data.status === 'ok') {
			showMessage({
				type: 'success',
				message: user.data.msg,
			});
		} else {
			showMessage({
				type: 'danger',
				message: user.data.msg,
			});
		}
		setLoad(false);
	}

	return(
		<View style={{
			paddingHorizontal: 20
		}}>
			<LoadingScreen enabled={load}/>
			<TextField
				label='Digite seu email'
				tintColor={colors.primary}
				autoCapitalize="none"
				autoCorrect={false}
				autoCompleteType="email"
				keyboardType={'email-address'}
				value={email}
				onChangeText={setEmail}
				style={styles.textFieldText}
				titleTextStyle={styles.textFieldText}
				labelTextStyle={styles.textFieldText}
			/>
			<ButtonDefault 
				buttonLabel={'Enviar email'}
				onSubmit={setSavePassword}
			/>
			<Text>
				Um email será enviado para a sua conta
			</Text>
			<Text>
				O email de redefinição de senha estará disponivel até 10 min, depois terá que reenviar outro
			</Text>
			<Text>
				Após redefinir a senha, apenas poderá enviar mensagem para redefinir senha após 1hr
			</Text>
		</View>
	)
}