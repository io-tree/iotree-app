import React, { useState, useEffect } from 'react';
import {
	View,
	Text
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import { TextField } from 'react-native-material-textfield';

import api from '../../../services/api';
import colors from '../../../globalStyles/colors';
import styles from './styles';
import LoadingScreen from '../../../components/LoadingScreen';
import ButtonDefault from '../../../components/ButtonDefault';
import { showMessage } from 'react-native-flash-message';

export default function ChangePassword({navigation}) {

	const [ currentPassword, setCurrentPassword ]= useState(undefined);
	const [ newPassword, setNewPassword ]= useState(undefined);
	const [ confirmPassword, setConfirmPassword ]= useState(undefined);

	const [ load, setLoad ] = useState(true);
	const [ id, setId ] = useState('');

	useEffect( () => {
		setLoad(false);
	}, []);

	async function setSavePassword() {
		setLoad(true); 
		if(newPassword === undefined || newPassword !== confirmPassword) {
			showMessage({
				type: 'warning',
				message: 'Senhas inconsistentes'
			})
			setLoad(false)
			return;
		}
		let user = await api.put('/users/'+navigation.getParam('id'),{
			password: currentPassword,
			password_new: newPassword
		});

		if(user.data.status === 'ok') {
			showMessage({
				type: 'success',
				message: user.data.msg,
			});
		} else {
			showMessage({
				type: 'warning',
				message: user.data.msg,
			});
		}
		setLoad(false);
	}

	return(
		<View style={{
			paddingHorizontal: 20
		}}>
			<LoadingScreen enabled={load}/>
			<TextField
				label='Senha atual'
				tintColor={colors.primary}
				autoCapitalize="none"
				secureTextEntry={true}
				autoCorrect={false}
				autoCompleteType="password"
				value={currentPassword}
				onChangeText={setCurrentPassword}
				style={styles.textFieldText}
				titleTextStyle={styles.textFieldText}
				labelTextStyle={styles.textFieldText}
			/>
			<TextField
				label='Senha nova'
				tintColor={colors.primary}
				autoCapitalize="none"
				secureTextEntry={true}
				autoCorrect={false}
				autoCompleteType="password"
				value={newPassword}
				onChangeText={setNewPassword}
				style={styles.textFieldText}
				titleTextStyle={styles.textFieldText}
				labelTextStyle={styles.textFieldText}
			/>
			<TextField
				label='Confirme nova senha'
				tintColor={colors.primary}
				autoCapitalize="none"
				secureTextEntry={true}
				autoCorrect={false}
				autoCompleteType="password"
				value={confirmPassword}
				onChangeText={setConfirmPassword}
				style={styles.textFieldText}
				titleTextStyle={styles.textFieldText}
				labelTextStyle={styles.textFieldText}
			/>
			<ButtonDefault 
				buttonLabel={'Mudar senha'}
				onSubmit={setSavePassword}
			/>
		</View>
	)
}