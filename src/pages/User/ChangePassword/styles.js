import colors from '../../../globalStyles/colors'
import fonts from '../../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

let menuSheetIconSize = 40;

const styles = StyleSheet.create({
  	container: {
		flex: 1
	},

  	textFieldsContainer:{
		marginTop:height/10
	},

  	textField: {
		marginTop: height/35,
		paddingHorizontal:width/10
	},
	
	textFieldText:{
		fontFamily:fonts.medium,
		color:colors.black
	},
  

});
export default styles;