import colors from '../../../globalStyles/colors'
import fonts from '../../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

let menuSheetIconSize = 40;

const styles = StyleSheet.create({
  container: {
		flex: 1
	},

  textFieldsContainer:{
		marginTop:height/10
	},

  	textField: {
		marginTop: height/35,
		paddingHorizontal:width/10
	},
	
	textFieldText:{
		fontFamily:fonts.medium,
		color:colors.black
	},
  
	buttonContainer:{
		alignItems: 'center'
	},

	button: {
		alignItems: 'center',
		height: height/15,
		borderRadius: 15,
		marginTop: height/15,
		backgroundColor: colors.primaryDark,
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: width/10,
		borderWidth:1,
		borderColor:colors.black,
		elevation:3,
		marginBottom:height/20
	},

 	buttonText: {
		fontFamily:fonts.medium,
		fontSize: fonts.l,
		color: colors.white,
	},

  	footer: {
		marginTop:height/15,
		paddingHorizontal:height/30,
		flex: 1,
		justifyContent: 'center',
		alignSelf: 'center',
	},

  	footerText: {
		fontFamily:fonts.medium,
		textAlign: 'center',
		color: colors.grayDark,
		fontSize: fonts.m,
		marginBottom: height/80
	},

	containerImage: {
		flex: 1,
		justifyContent: 'center',
		marginBottom: 40,
	},

	changeImage: {
		textAlign: 'center'
	},

	profileImage: {
		marginTop:10,
		width: 150,
		height: 150,
		marginBottom: 10,
		borderRadius: 75,
		alignSelf: "center"
	},

	imageContent: {
		alignSelf: "center",
		width: 150,
		height: 150
	},

	iconEdit: {
		position: 'absolute',
		bottom: -10,
		right: -10,
		width: 50,
		height: 50,
		borderRadius: 25,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: colors.primaryDark
	},

	lineEdit: {
		flex: 1,
		flexDirection: 'row',
		alignSelf: 'flex-start',
		alignItems: 'center',
		marginBottom: 5,
		paddingVertical: 5,
	},

	lineEditIcon: {
		width: 30, 
		height: 30, 
		justifyContent: 'center', 
		alignItems: 'center'
	},
	
	
	editText: {
		flex: 1,
		fontFamily:fonts.regular,
		fontSize:fonts.m,
		color:colors.black,
		paddingVertical: 5,
		textAlign: "left",
		marginLeft: 10,
	},
	
	editTextEnabled: {
		flex: 1,
		fontFamily:fonts.regular,
		fontSize:fonts.m,
		color:colors.black,
		textAlignVertical:'center',
		marginLeft: 10,
		borderBottomWidth: 1,
		borderBottomColor: colors.primaryDark,
		paddingVertical: 5
	},

	textButtonChangePassword: {
		flex: 1,
		marginLeft: 15,
		fontFamily:fonts.regular,
		fontSize:fonts.m,
		color:colors.black,
	},

	menuSheetContainer: {
		justifyContent: "flex-start",
		alignItems: "center",
		flexDirection: "row",
	},

	menuSheet: {
		alignItems: 'center',
		marginLeft: 35
	},

	menuSheetIconContainer: {
		backgroundColor: colors.primaryDark,
		alignItems: "center",
		justifyContent: 'center',
		width: menuSheetIconSize,
		height: menuSheetIconSize,
		borderRadius: menuSheetIconSize
	},

	menuSheetText: {
		fontSize: 11,
		marginTop: 6
	},

	modalContainer: {
		backgroundColor: 'white',
		width: '70%',
		borderRadius: 0
	},

	modalInfo: {
		padding: 10
	},

	modalButtonContainer: {
		width: '100%',
		flexDirection: 'row',
		justifyContent: "space-around"
	},

	modalButton: {
		height: 30,
		backgroundColor: colors.primaryDark,
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},

	modalInput: {
		borderBottomWidth: 1,
		paddingVertical: 5,
		paddingLeft: 10,
		borderColor: colors.primaryDark,
		fontFamily:fonts.regular,
		fontSize:fonts.m,
	},

	modalButtonText: {
		color: 'white'
	}
});
export default styles;