import React, { useState, useEffect, useCallback, useRef, Children } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Modal,
  Image
} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import ImagePicker from 'react-native-image-crop-picker';
import RBSheet from "react-native-raw-bottom-sheet";
import { showMessage } from "react-native-flash-message";
import moment from 'moment';

import api from '../../../services/api';
import colors from '../../../globalStyles/colors';
import styles from './styles';
import exceptions from '../../../services/exceptions';
import AsyncStorage from '@react-native-community/async-storage';
import { TextInput } from 'react-native-gesture-handler';

import LoadingScreen from '../../../components/LoadingScreen';
import GeneralSelect from '../../../components/GeneralSelect';
import DatePicker from '../../../components/DatePicker';
import ButtonDefault from '../../../components/ButtonDefault';
import ModalAction from '../../../components/ModalAction';

export default function EditUser ({ navigation }) {

  const [menuButton, setMenuButton] = useState('');
  const [email, setEmail] = useState('');
  const [birthday_date, setBirthday_date] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [user, setUser] = useState({});
  const [load, setLoad] = useState(true);
  const [gender, setGender] = useState('');
  const [saveUser, setSaveUser] = useState(false);
  const [updateUser, setUpdateUser] = useState(false);


  useEffect(() => {
    (async () => {
      let user = await api.get('/users/' + navigation.getParam('id'));
      user = user.data.body;
      await AsyncStorage.setItem('user', JSON.stringify(user));
      setUser(user);
      setEmail(user.email);
      setName(user.name);
      setBirthday_date(user.birthday_date);
      setGender(user.id_gender);
      setLoad(false);
      setUpdateUser(false)
    })();
  }, [updateUser])

  function selectImage () {
    ImagePicker.openPicker({
      mediaType: 'photo',
      includeBase64: true,
    }).then(image => {
      cropImage(image);
    }).catch(err => {
      console.log(err)
    });

  }

  function takeCamera () {
    ImagePicker.openCamera({
      mediaType: 'photo',
      includeBase64: true,
    }).then(image => {
      cropImage(image);
    }).catch(err => {
      console.log(err)
    });;
  }


  function cropImage (img) {
    ImagePicker.openCropper({
      path: img.path,
      includeBase64: true,
      width: 400,
      height: 400,
    }).then(image => {
      onChangeImage(image)
    }).catch(err => {
      console.log(err)
    });
  }

  function removeImage () {
    onChangeImage();
  }

  async function onChangeImage (image = null) {
    setLoad(true);
    if (image === null) {
      let userU = await api.delete('/uploads', {
        params: {
          id: user.id_user,
          mod: 'users'
        }
      });
      if (userU.data.status === 'ok') {
        showMessage({
          type: 'success',
          message: 'Foto de perfil removida'
        });
        setUpdateUser(true);
      } else {
        showMessage({
          type: 'info',
          message: 'Foto de perfil já removida'
        });
        setLoad(false)
      }
    } else {

      let form = new FormData();

      let name = image.path.split('/')
      form.append('img', {
        uri: image.path,
        type: image.mime,
        name: name[name.length - 1]
      });

      form.append('path', 'users');
      form.append('id', user.id_user);

      let res = await api.post('/uploads', form, {
        onUploadProgress: progressCallback,

      });

      if (res.data.status === 'ok') {
        showMessage({
          type: 'success',
          message: 'Foto de perfil atualizada'
        });
        setUpdateUser(true);
      } else {
        showMessage({
          type: 'danger',
          message: 'Não foi possivel enviar a imagem'
        })
      }
    }
  }

  function onPressSave () {
    setSaveUser(true);
  }

  async function onSubmit (password) {
    setLoad(true);
    let user = await api.put('/users/' + navigation.getParam('id'), {
      name,
      email,
      password,
      birthday_date,
      id_gender: gender
    });

    if (user.data.status === 'ok') {
      showMessage({
        type: 'success',
        message: user.data.msg,
      });
    } else {
      showMessage({
        type: 'error',
        message: user.data.msg,
      });
    }
    setUpdateUser(true);
  }

  const progressCallback = progressEvent => {
    const percentFraction = progressEvent.loaded / progressEvent.total;
    const percent = Math.floor(percentFraction * 100);
  }

  return (
    <View style={styles.container}>
      <LoadingScreen enabled={load} />
      <ConfirmPasswordModal
        enabled={saveUser}
        setEnabled={setSaveUser}
        onSetPassword={onSubmit}
      />
      {!load &&
        <ScrollView>
          <View style={styles.containerImage}>
            <View style={styles.imageContent}>
              <Image
                style={styles.profileImage}
                source={{
                  uri: api.defaults.baseURL + user.path_img + '?random_number=' + new Date().getTime(),
                }}
              />
              <TouchableOpacity style={styles.iconEdit} onPress={() => menuButton.open()}>
                <FontAwesomeIcon name={'pen'} color={'white'} size={15} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{
            marginHorizontal: 40,
          }}>
            <EditLabel icon={'user-circle'} value={name} onChangeText={setName} />
            <EditLabel icon={'envelope'} value={email} onChangeText={setEmail} />
            <EditLabel
              icon={'calendar-alt'}
            >
              <DatePicker
                labelText={'Data de Nascimento'}
                setDate={setBirthday_date}
                labelSelect={'Selecione sua data de nascimento'}
                date={new Date(birthday_date)}
                type={2}
              />
            </EditLabel>
            <EditLabel
              icon={'venus-mars'}
            >
              <GeneralSelect
                idSelected={gender}
                setIdSelected={setGender}
                type={2}
                model='gender'
              />
            </EditLabel>
            <EditLabel icon={'lock'}>
              <Text
                onPress={() => navigation.navigate('ChangePassword', { id: user.id_user })}
                style={styles.textButtonChangePassword}
              >
                Mudar senha
							</Text>
            </EditLabel>

            <ButtonDefault buttonLabel={'Salvar'} onSubmit={onPressSave} />

          </View>

          <RBSheet
            ref={ref => {
              setMenuButton(ref);
            }}
            height={100}
            duration={250}
            customStyles={{ container: styles.menuSheetContainer }}
          >
            <MenuSheet iconName={'trash'} iconText={'Remover imagem'} func={removeImage} />
            <MenuSheet iconName={'camera'} iconText={'Câmera'} func={takeCamera} />
            <MenuSheet iconName={'images'} iconText={'Galeria'} func={selectImage} />
          </RBSheet>



        </ScrollView>
      }
    </View>
  );

  function MenuSheet ({ iconName, iconText, func }) {
    return (
      <TouchableOpacity style={styles.menuSheet} onPress={() => { func(); menuButton.close() }}>
        <View style={styles.menuSheetIconContainer}>
          <FontAwesomeIcon name={iconName} color={'white'} size={15} />
        </View>
        <Text style={styles.menuSheetText}>{iconText}</Text>
      </TouchableOpacity>
    )
  }
}

function EditLabel ({ icon, value, onChangeText, children }) {

  const [enabled, setEnabled] = useState(false);

  return (
    <View style={styles.lineEdit}>
      <View style={styles.lineEditIcon}>
        <FontAwesomeIcon name={icon} color={colors.primaryDark} size={20} />
      </View>
      {children ?
        children
        :
        <TextInput
          selectTextOnFocus={true}
          editable={true}
          autoFocus={enabled}
          style={(enabled ? styles.editTextEnabled : styles.editText)}
          onChangeText={text => onChangeText(text)}
          onBlur={() => setEnabled(false)}
          onFocus={() => setEnabled(true)}
          value={value}
        />
      }
      <View style={{ width: 20, height: 20 }}>
        <FontAwesomeIcon name={'pen'} color={colors.primaryDark} size={15} />
      </View>
    </View>
  )
}

function ConfirmPasswordModal ({ enabled, setEnabled, onSetPassword }) {

  const [pass, setPass] = useState('');

  return (
    <ModalAction
      enabled={enabled}
      onClose={() => {
        setEnabled(false);
        setPass('');
      }}
      listAction={[
        {
          label: 'Ok',
          function: () => onSetPassword(pass)
        },
        {
          label: 'Cancelar'
        }
      ]}
    >
      <Text>Confirme sua senha</Text>
      <TextInput
        autoFocus={true}
        textContentType={'password'}
        secureTextEntry={true}
        style={styles.modalInput}
        onChangeText={text => setPass(text)}
        value={pass}
      />
    </ModalAction>
  )
}