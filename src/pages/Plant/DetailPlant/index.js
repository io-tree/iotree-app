import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions } from 'react-navigation';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { showMessage } from "react-native-flash-message";

import styles from './styles';
import colors from '../../../globalStyles/colors';
import api from '../../../services/api';

import LoadingScreen from '../../../components/LoadingScreen';
import ButtonDefault from '../../../components/ButtonDefault';

const iconSizeFontAwesome = 35;
const iconSizeMaterial = 45;

export default function DetailPlant ({ navigation }) {

  const [loading, setLoading] = useState(true);
  const [plant, setPlant] = useState([]);
  const [canPlant, setCanPlant] = useState(false);

  let didWilFocus = null;

  useEffect(() => {
    (async () => {
      await init();
    })();

    didWilFocus = navigation.addListener('willFocus', async () => await init());

  }, []);

  useEffect(() => () => {
    didWilFocus.remove();
  }, []);

  async function init () {
    let id_plant = navigation.getParam('id_plant');
    let namePlant = navigation.getParam('name');

    let plant = await api.get(`/plants?plant=${id_plant}`);

    if (plant.data.body.length > 0) {
      console.log(plant.data)
      setPlant(plant.data.body[0]);

      navigation.setParams({ title: namePlant });

      let user = await AsyncStorage.getItem('user');

      if (user) {
        user = JSON.parse(user);

        const greenhouses = await api.get(`/users/${user.id_user}/greenhouses`);
        if (greenhouses.data.status === 'ok') {
          if (greenhouses.data.body.filter(greenhouse => !greenhouse.plantation).length > 0) {
            setCanPlant(true)
          } else {
            setCanPlant(false)
          }
        }
      }
    } else {
      showMessage({
        type: 'danger',
        message: 'Não foi visualizar a planta'
      })
      return navigation.goBack()
    }

    setLoading(false);
  }

  return (
    <View style={{ flex: 1, alignItems: 'center' }}>
      <LoadingScreen enabled={loading} />
      {!loading &&
        <ScrollView>
          <View style={styles.container}>
            <Text style={styles.title}>{plant.scientific_name}</Text>
            <Image
              style={styles.image}
              source={{
                uri: api.defaults.baseURL + plant.path_img
              }}
            />

            <View style={styles.boxDetail}>
              <BoxDetail
                icon={<FontAwesomeIcon name={'temperature-high'} color={colors.primary} size={iconSizeFontAwesome} />}
                name={`${plant.temperature.min}°C`}
              />
              <BoxDetail
                icon={<MaterialCommunityIcon name={'water-percent'} color={colors.primary} size={iconSizeMaterial} />}
                name={`${plant.humidity_air.min}%`}
              />
              <BoxDetail
                icon={<FontAwesomeIcon name={'ruler-vertical'} color={colors.primary} size={iconSizeFontAwesome} />}
                name={`Altura máxima ${plant.height.max}cm`}
              />
              <BoxDetail
                icon={<MaterialCommunityIcon name={'water-outline'} color={colors.primary} size={iconSizeMaterial} />}
                name={`pH ${plant.pH.min}%`}
              />
              <BoxDetail
                icon={<MaterialCommunityIcon name={'update'} color={colors.primary} size={iconSizeMaterial} />}
                name={`Colheita ${plant.cycle.min} dias`}
              />
            </View>
            <Text style={styles.boxDescription}>{plant.description}</Text>
          </View>

          {canPlant &&
            <ButtonDefault
              onSubmit={() => {
                navigation.navigate('Plantation', { id_plant: navigation.getParam('id_plant') }, NavigationActions.navigate({ routeName: 'Create', params: { id_plant: navigation.getParam('id_plant') } }))
              }}
              buttonLabel={'Plantar'}
              buttonType={2}
              textType={2}
            />
          }
        </ScrollView>
      }
    </View>
  )
}

function BoxDetail ({ icon, name }) {
  return (
    <View style={styles.boxDetailItem}>
      {icon}
      <Text style={styles.boxDetailItemText}>{name}</Text>
    </View>
  )
}