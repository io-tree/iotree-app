import {
	StyleSheet
} from 'react-native';
import colors from '../../../globalStyles/colors';

const boxSize = 200;
const boxSizeDetail = 95;

export default StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		alignSelf: 'stretch',
		justifyContent: 'center',
		paddingHorizontal: 15,
		flexWrap: 'wrap',
	},

	title: {
		fontStyle: 'italic'
	},

	image: {
		width: boxSize,
		height: boxSize,
		alignSelf: "center",
		borderRadius: 20,
		marginTop: 45
	},

	boxDetail: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		flexWrap: 'wrap',
		padding: 5,
		marginTop: 20
	},

	boxDetailItem: {
		padding: 5,
		margin: 10,
		width: boxSizeDetail,
		height: boxSizeDetail,
		justifyContent: 'space-evenly',
		alignItems: 'center',
		borderRadius: 8,
		borderWidth: 1,
		borderColor: colors.gray,
		backgroundColor: 'white',
		elevation: 3
	},

	boxDetailItemText: {
		marginTop: 5, 
		fontSize: 12, 
		textAlign: 'center'
	},

	boxDescription: {
		fontSize: 15,
		marginTop: 15,
		textAlign: 'justify'
	}
}) 