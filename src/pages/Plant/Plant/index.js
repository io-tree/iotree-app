import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity
} from 'react-native';

import styles from './styles';
import colors from '../../../globalStyles/colors';
import api from '../../../services/api';

import LoadingScreen from '../../../components/LoadingScreen';
import ScrollViewRefresh from '../../../components/ScrollViewRefresh';

export default function Plant ({ navigation }) {

  const [loading, setLoading] = useState(true);
  const [listPlant, setListPlant] = useState([]);

  useEffect(() => {
    (async () => {
      await init();
      setLoading(false);
    })();
  }, []);

  async function init () {
    let id_category = navigation.getParam('id_category');
    let listPlant = await api.get(`/plants?category=${id_category}`);
    setListPlant(listPlant.data.body);
  }

  return (
    <View>
      <LoadingScreen enabled={loading} />
      <ScrollViewRefresh
        onRefresh={
          () => init()
        }
      >
        <View style={styles.container}>
          {!loading &&
            listPlant.map((plant, index) => {
              return (
                <TouchableOpacity key={index} style={styles.boxPlant} onPress={() => navigation.navigate('DetailPlant', { id_plant: plant.id_plant, name: plant.name })}>
                  <Image
                    resizeMode={'cover'}
                    source={{ uri: api.defaults.baseURL + plant.path_img + '?random_number=' + new Date().getTime() }}
                    style={styles.boxImage}
                  />
                  <View style={styles.boxTextContainer} >
                    <Text style={styles.boxTextTitle} >{plant.name}</Text>
                    {/* <Text style={styles.boxTextSubtitle} >{plant.description}</Text> */}
                  </View>
                </TouchableOpacity>
              )
            })
          }
          {loading || listPlant.length === 0 &&
            <Text>Sem Plantas</Text>
          }
        </View>
      </ScrollViewRefresh>
    </View>
  )
}