import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  InteractionManager
} from 'react-native';

import styles from './styles';
import colors from '../../../globalStyles/colors';
import api from '../../../services/api';

import LoadingScreen from '../../../components/LoadingScreen';
import ScrollViewRefresh from '../../../components/ScrollViewRefresh';

export default function Category ({ navigation }) {

  const [loading, setLoading] = useState(true);
  const [listCategories, setListCategories] = useState([]);

  useEffect(() => {
    (async () => {
      await init()
      setLoading(false);
    })();
  }, []);

  async function init () {
    let categories = await api.get('/categories');
    setListCategories(categories.data.body);
  }

  return (
    <View>
      <LoadingScreen enabled={loading} />
      <ScrollViewRefresh
        onRefresh={
          () => init()
        }
      >
        <View style={styles.container}>
          {!loading &&
            listCategories.map((category, index) => {
              return (
                <TouchableOpacity key={index} style={styles.boxCategory} onPress={() => navigation.navigate('Plant', { id_category: category.id_category })}>
                  <Image
                    resizeMode={'cover'}
                    source={{ uri: api.defaults.baseURL + category.path_img + '?random_number=' + new Date().getTime() }}
                    style={styles.boxImage}
                  />
                  <View style={styles.boxTextContainer} >
                    <Text style={styles.boxTextTitle} >{category.name}</Text>
                    <Text style={styles.boxTextSubtitle} >{category.description}</Text>
                  </View>
                </TouchableOpacity>
              )
            })
          }
          {loading || listCategories.length === 0 &&
            <Text>Sem categorias</Text>
          }
        </View>
      </ScrollViewRefresh>
    </View>
  )
}