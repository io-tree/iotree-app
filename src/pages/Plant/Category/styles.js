import {
	StyleSheet
} from 'react-native';
import colors from '../../../globalStyles/colors';

const boxSize = 140;

export default StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		alignSelf: 'stretch',
		justifyContent: 'space-evenly',
		flexDirection: 'row',
		padding: 15,
		flexWrap: 'wrap',
	},

	boxImage: {
		width: boxSize,
		height: boxSize - boxSize / 2.7,
		alignSelf: "center",
	},

	boxCategory: {
		width: boxSize,
		height: boxSize,
		borderRadius: 15,
		borderWidth: 1,	
		borderColor: colors.gray,
		overflow: 'hidden',
		marginBottom: 18
	},

	boxTextContainer: {
		padding: 5,
		flex: 1,
		justifyContent: 'center',
	},

	boxTextTitle: {
		fontSize: 14
	},

	boxTextSubtitle: {
		fontSize: 11
	}
}) 