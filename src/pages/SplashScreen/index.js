import React, { useEffect, useState } from 'react';
import {
	View,
	Text,
	Animated
} from 'react-native';
import Splash from 'react-native-splash-screen'
import NetInfo from "@react-native-community/netinfo";
import { showMessage } from "react-native-flash-message";
import LottieView from 'lottie-react-native';
import AsyncStorage from '@react-native-community/async-storage';

import api from '../../services/api';
import socket from '../../services/socket';
import exceptions from '../../services/exceptions';
import { loads, loadSelected } from '../../services/jsonLoading';
import styles from './styles';


export default function SplashScreen({ navigation }) {

	const [ info, setInfo ] = useState('Oi')
	const [ loop, setLoop ] = useState(true);
	const [ welcome, setWelcome ] = useState('Bem vindo');

	global.language = 'pt-br';

	useEffect( () => {
		Splash.hide();

		(async () => {
			//await AsyncStorage.clear();

			let isConnected = await getNetwork();
			
			if(!isConnected) return setInfo('Sem conexão com internet');

			let statusServer = await getServerConnection();

			if(!statusServer) return setInfo('Problemas no servidor');

			if(!sendConnection()){
				return setInfo('Problemas no websocket');
			}
			
			await getUser();
			

			setInfo('Tudo pronto, iniciando app');
			setLoop(false);
		})();
		
		
	}, []);

	async function getNetwork() {
		setInfo('Verificando conexção com a internet');
		let network = await NetInfo.fetch();
		if (network && network.isConnected) return true;
		return false;
	}


	async function getServerConnection(network){
		setInfo('Verificando conexão com o servidor');

		try {
			let serverStatus = await api.get('/');

			if (serverStatus.data.status === 'ok') return true;
		} catch(err) {
			showMessage(exceptions(err));
		}
		
		return false;
	}

	async function getUser(){
		const keyConnection = await AsyncStorage.getItem('keyConnection');

		if (keyConnection) {
			setInfo('Restaurando sessão');
			const res = await api.post('/reconnect/',{
				keyConnection
			});
			if(res.data.status === 'ok') {
				setWelcome('Bem vindo de volta, ' + res.data.body.name.split(' ')[0]);
				await AsyncStorage.setItem('keyConnection', res.data.keyConnection);
				await AsyncStorage.setItem('token', res.data.token);
				await AsyncStorage.setItem('refreshToken', res.data.refreshToken);
			} else {
				await AsyncStorage.clear();
			}
		} else {
			await AsyncStorage.clear();
		}
	}

	function sendConnection(){
		socket.emit('verify_user', 'ok');
		return socket.on(socket.id, (data) => {
			if(data === 'ok') {
				return true;
			}
			return false;
		});
	}

	return (
		<View style={styles.container}>
			<LottieView
				source={loads[loadSelected][0]}
				autoPlay
				loop={loop}
				speed={loads[loadSelected][1]}
				onAnimationFinish={() => {navigation.navigate('App')}}
				style={{
					width: loads[loadSelected][2]
				}}
			/>
			<View style={styles.info}>
				<Text style={styles.text}>
					{welcome}
				</Text>
				<Text style={styles.text}>
					{info}
				</Text>
			</View>
		</View>
	)
}