import colors from '../../globalStyles/colors'
import fonts from '../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.primaryDark,
		flexDirection: 'column'
	},

	info: {
		position: "absolute",
		top: '70%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	text: {
		color: 'white'
	}

});
export default styles;