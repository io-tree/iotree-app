import React, { useEffect, useState } from 'react';
import { View, Text, ScrollView, Platform, Image, Dimensions } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import ImagePicker from 'react-native-image-crop-picker';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5'
import { TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import FormData from 'form-data';
import { showMessage } from "react-native-flash-message";
import api from '../../../services/api';

import styles from './styles';
import colors from '../../../globalStyles/colors';

import LoadingScreen from '../../../components/LoadingScreen';
import ViewAuthorization from '../../../components/ViewAuthorization';

export default function CreatePost ({ navigation }) {

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [images, setImages] = useState([0]);
  const [carouselIndex, setCarouselIndex] = useState(0);
  const [loading, setLoading] = useState(false);
  const [pass, setPass] = useState(false)

  const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

  function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
  }

  const slideHeight = viewportHeight * 0.36;
  const slideWidth = wp(75);
  const itemHorizontalMargin = wp(2);

  const sliderWidth = viewportWidth;
  const itemWidth = slideWidth + itemHorizontalMargin * 2;

  useEffect(() => {
    (async function () {
      let user = await AsyncStorage.getItem('user');
      if (user) {
        setPass(true)
      } else {
        setPass(false)
      }
    })()
  }, [])

  function selectImage () {
    ImagePicker.openPicker({
      multiple: true,
      mediaType: 'photo',
      includeBase64: true
    }).then(imgs => {
      let temp = images;
      if (temp.length - 1 + imgs.length >= 5) {
        temp.pop();
        imgs.length = 5 - temp.length;
        setImages([...temp, ...imgs])
      } else {
        if (temp.length > 1) {
          temp.pop();
          setImages([...temp, ...imgs, 0])
        } else {
          setImages([...imgs, 0])
        }
      }

    }).catch(err => {
      console.log(err)
    });

  }


  function cropImage (img) {
    ImagePicker.openCropper({
      path: img.path,
      includeBase64: true
    }).then(image => {
      setImages([0])
      let temp = images;
      temp[carouselIndex] = image;
      setImages(temp)
    }).catch(err => {
      console.log(err)
    });
  }

  function removeImage () {
    setImages([0])
    let temp = images;
    if (temp.includes(0))
      temp.pop();
    temp.splice(carouselIndex, 1);
    setImages([...temp, 0])
  }

  const progressCallback = progressEvent => {
    const percentFraction = progressEvent.loaded / progressEvent.total;
    const percent = Math.floor(percentFraction * 100);
  }

  async function onSubmit () {

    setLoading(true);

    let user = JSON.parse(await AsyncStorage.getItem('user'));

    if (title.length === 0 && !title.match(/[a-zA-Z0-9]/g)) {
      showMessage({
        type: 'danger',
        message: 'O titulo está vazio'
      })
      setLoading(false);
      return;
    } else if (description.length === 0 && !description.match(/[a-zA-Z0-9]/g)) {
      showMessage({
        type: 'danger',
        message: 'A descrição está vazia'
      })
      setLoading(false);
      return;
    }

    let post = (await api.post('/users/' + user.id_user + '/posts', {
      title,
      description
    })).data;

    let form = new FormData();

    if (images.includes(0)) images.pop();
    images.map(img => {
      let name = img.path.split('/')
      form.append('img', {
        uri: img.path,
        type: img.mime,
        name: name[name.length - 1]
      });
    });

    form.append('path', 'posts');
    form.append('id', post.body.id_post);

    api.post('/uploads', form, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer ' + post.token,
      },
      onUploadProgress: progressCallback,
    }).then(async res => {
      showMessage({
        type: 'success',
        message: 'Post enviado'
      })
      navigation.goBack();
      setLoading(false);
    }).catch(err => {
      setLoading(false);
      showMessage({
        type: 'danger',
        message: 'Não foi possivel enviar o post'
      })
    })

  }

  return (
    <ViewAuthorization
      navigation={navigation}
    >
      <ScrollView>
        <LoadingScreen enabled={loading} />
        <View style={styles.container}>
          <TextField
            label='Titulo'
            title='Digite um titulo'
            tintColor={colors.primary}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            autoCompleteType="off"
            value={title}
            onChangeText={setTitle}
          />
          <TextField
            label='Descrição'
            title='Digite uma descrição'
            tintColor={colors.primary}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            autoCompleteType="off"
            value={description}
            onChangeText={setDescription}
          />
          <View style={{ marginVertical: 15, alignItems: 'center' }}>
            <Carousel
              inactiveSlideOpacity={0.3}
              inactiveSlideScale={0.34}
              slideStyle={styles.carousel}
              contentContainerCustomStyle={styles.carouselContainer}
              data={images}
              renderItem={({ item }) => (<Item image={item} height={viewportWidth} width={itemWidth} crop={cropImage} remove={removeImage} add={selectImage} />)}
              sliderWidth={viewportWidth}
              itemWidth={itemWidth}
              onSnapToItem={(index) => setCarouselIndex(index)}

            />
            <Text>Clique na imagem para editar</Text>
            <Text>Segure para apagar</Text>
            <Pagination
              dotsLength={images.length}
              activeDotIndex={carouselIndex}
              containerStyle={styles.paginationContainer}
              dotColor={colors.primaryDark}
              dotStyle={styles.paginationDot}
              inactiveDotColor={'gray'}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
              animatedDuration={0}
              animatedTension={0}
            />
          </View>
          <TouchableOpacity style={styles.sendPost} onPress={onSubmit}>
            <Text style={{ fontSize: 20, color: 'white' }}>Enviar post</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </ViewAuthorization>
  )
}


function Item ({ image, width, height, crop, remove, add }) {
  return (
    (image !== 0) ?
      <View>
        <TouchableWithoutFeedback onPress={() => crop(image)} onLongPress={remove}>
          <Image style={{ height: height, width: width, resizeMode: 'contain' }} source={{ uri: `data:${image.mime};base64,${image.data}` }} />
        </TouchableWithoutFeedback>
      </View>
      :
      <TouchableOpacity style={{ ...styles.selectImage, height: height, width: width }} onPress={add}>
        <FontAwesomeIcon name="plus-circle" size={80} style={styles.selectImageIcon} />
        <Text>Adicionar imagem</Text>
      </TouchableOpacity>
  )


}