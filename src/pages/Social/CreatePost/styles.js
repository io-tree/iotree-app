import colors from '../../../globalStyles/colors'
import fonts from '../../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  	container: {
		flex: 1,
		justifyContent: 'center',
		paddingHorizontal: 30
	},

	carousel: {
		alignItems: 'center', 
		backgroundColor: 'rgba(0,178,72,0.4)', 
		justifyContent: 'center', 
		borderRadius: 10,
		padding: 10
	},

	carouselContainer: {
		alignItems: 'center',
		justifyContent: 'center'
	},	

	paginationContainer: {
		paddingTop: 30,
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 8
	},
	
	selectImage: {
		padding: 15,
		alignItems: 'center', 
		borderRadius: 10, 
		justifyContent: 'center', 
	},

	selectImageIcon: {
		color: 'white'
	},

	sendPost: {
		marginVertical: 20,
		padding: 20,
		backgroundColor: colors.primaryDark,
		borderRadius: 10,
		justifyContent: "center",
		alignItems: 'center'
	}
});
export default styles;