import React, { useState, useEffect, useCallback } from 'react';
import {
  View,
  ActivityIndicator,
  Text,
  TextInput,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Image,
  Modal,
} from 'react-native'
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';

import styles from './styles'
import api from '../../../services/api';
import colors from '../../../globalStyles/colors';
import { showMessage } from 'react-native-flash-message';

export default function Social ({ navigation }) {
  const [feed, setFeed] = useState([]);
  const [comment, setComment] = useState([]);
  const [textComment, setTextComment] = useState('');
  const [readyComment, setReadyComment] = useState(false);
  const [idPost, setIdPost] = useState(0);
  const [openModal, setOpenModal] = useState(false);
  const [page, setPage] = useState(1);
  const [user, setUser] = useState({});
  const [totalPage, setTotalPage] = useState(0);
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefresing] = useState(false);
  const [carouselIndex, setCarouselIndex] = useState(0);

  const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

  function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
  }

  const slideHeight = viewportHeight * 0.36;
  const slideWidth = wp(75);
  const itemHorizontalMargin = wp(2);

  const sliderWidth = viewportWidth;
  const itemWidth = slideWidth + itemHorizontalMargin * 2;

  async function loadPage (pageNumber = page, shouldRefresh = false) {
    if (totalPage && pageNumber > totalPage) return;

    setLoading(true);

    const res = await api.get('/posts?page=' + pageNumber);

    setTotalPage(Math.ceil(res.data.body.count / 5));
    setPage(pageNumber + 1);
    setFeed(shouldRefresh ? res.data.body.rows : [...feed, ...res.data.body.rows])

    setLoading(false);

  }

  function updateFeed (index, id_post, remove = false) {
    if (!remove) {
      api.get('/posts?post=' + id_post).then(res => {
        let ps = res.data.body.rows[0];
        let newArr = [...feed];
        newArr[index] = ps;
        setFeed(newArr);
      })
    } else {
      let newArr = [...feed];
      newArr.splice(index, 1);
      setFeed(newArr);
    }
  }

  function toogleModal () {
    setOpenModal(!openModal);
    if (idPost !== 0) {
      setIdPost(0)
      setComment([]);
      setReadyComment(false);
    }
  }

  function updateComments () {
    api.get('/posts?post=' + idPost).then(res => {
      let ps = res.data.body.rows[0];
      setComment(ps.comments);
      setReadyComment(true)
    })
  }

  useEffect(() => {
    if (idPost) updateComments()
  }, [idPost])

  useEffect(() => {
    const def = navigation.addListener('willFocus', init)
    init()
    return () => {
      def.remove()
    }
  }, [])

  function init () {
    setLoading(true);
    AsyncStorage.getItem('user').then(user => {
      if (user) {
        setUser(JSON.parse(user));
      } else {
        setUser({})
      }
      loadPage(1, true);
    })
  }

  async function refreshList () {
    setRefresing(true);

    await loadPage(1, true)

    setRefresing(false);
  }

  return (
    <View style={styles.container}>
      {feed.length > 0 ? (
        <FlatList
          data={feed}
          keyExtractor={post => post.id_post.toString()}
          renderItem={({ item, index }) =>
            <Item
              post={item}
              index={index}
              user={user}
              viewportWidth={viewportWidth}
              itemWidth={itemWidth}
              carouselIndex={carouselIndex}
              setCarouselIndex={setCarouselIndex}
              updateFeed={updateFeed}
              setIdPost={setIdPost}
              toogleModal={toogleModal}
            />
          }
          onEndReached={() => { loadPage() }}
          onEndReachedThreshold={0.1}
          onRefresh={refreshList}
          refreshing={refreshing}
          ListFooterComponent={loading && <ActivityIndicator size={'large'} color={colors.primaryDark} />}
        />
      ) : (
          <TouchableOpacity onPress={() => { loadPage(1, true) }}>
            {loading ?
              <ActivityIndicator size="large" color={colors.primaryDark} />
              :
              <View style={{ justifyContent: "center", alignItems: 'center' }}>
                <Text>Nenhum post para mostrar</Text>
                <View style={{
                  backgroundColor: colors.primaryDark,
                  width: 50,
                  height: 50,
                  borderRadius: 25,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 10
                }}>
                  <FontAwesomeIcon name="redo-alt" size={20} color={'white'} />
                </View>
              </View>
            }
          </TouchableOpacity>
        )}
      <Modal
        visible={openModal}
        animationType={"slide"}
        transparent={false}
        presentationStyle={"pageSheet"}
        onRequestClose={() => {
          toogleModal()
        }}
      >
        <View style={{ flex: 1, flexDirection: "column" }}>
          <View style={styles.comments}>
            <TouchableOpacity onPress={toogleModal} style={styles.commentsBack}>
              <FontAwesomeIcon name="times" size={30} color={colors.primaryDark} />
            </TouchableOpacity>
            <Text style={styles.commentsTitle}>Comentarios</Text>
          </View>
          <View style={{ flex: 1, padding: 10 }}>
            {readyComment ?
              comment ?
                <FlatList
                  keyExtractor={id => id.id_comment.toString()}
                  data={comment}
                  inverted={-1}
                  renderItem={({ item }) => <ItemComment item={item} />}
                />
                :
                <Text>Sem</Text>
              :
              <ActivityIndicator style={{ flex: 1, alignSelf: 'center' }} color={colors.primaryDark} size="large" />
            }
          </View>
          <View style={styles.commentsSendContainer}>
            {Object.keys(user).length ? (
              <>
                <TextInput
                  onChangeText={setTextComment}
                  value={textComment}
                  style={styles.commentsSendContainerInput}
                />
                <TouchableOpacity onPress={() => { createComment(user.id_user, idPost, textComment); setTextComment('') }}>
                  <MaterialCommunityIcons name="send" size={30} color={colors.primaryDark} />
                </TouchableOpacity>
              </>
            ) : (
                <Text>Logue para comentar</Text>
              )}
          </View>
        </View>
      </Modal>
    </View>
  )
}

function Item ({ post, index, user, viewportWidth, itemWidth, carouselIndex, setCarouselIndex, updateFeed, setIdPost, toogleModal }) {

  let iconSize = 18;
  let time = getTimeSend(post.createdAt)

  let reacts = {}

  reacts[post.id_post] = {
    id: 0,
    like: 0,
    dislike: 0,
    type: 0
  }

  post.reacts.map(l => {
    reacts[post.id_post].id++;
    if (l.id_react_type === 1) {
      reacts[post.id_post].like++;
    }
    if (l.id_react_type === 2) {
      reacts[post.id_post].dislike++;
    }
    if (l.id_user === user.id_user) {
      reacts[post.id_post].type = l.id_react_type
    }
  })

  return (
    <View style={styles.item}>
      <View style={styles.itemMenu}>
        <Text style={styles.itemTitle}>{post.title}</Text>
        {post.id_user === user.id_user && (
          <Menu>
            <MenuTrigger>
              <View style={{ padding: 10 }}>
                <FontAwesomeIcon name="ellipsis-v" size={20} />
              </View>
            </MenuTrigger>
            <MenuOptions customStyles={{ optionsContainer: { width: 'auto' } }}>
              <MenuOption style={styles.itemOptions} value={1} text='Editar' />
              <MenuOption style={styles.itemOptions} onSelect={() => deletePost(post.id_post, user, index, updateFeed)} text='Apagar' />
            </MenuOptions>
            {/* <MenuOptions customStyles={{optionsContainer: { alignSelf: 'flex-start', alignItems: 'center', width: 'auto'}}}>
							<View style={{flex: 1, flexDirection: 'row'}}>
								<MenuOption value={1} text='One' style={{backgroundColor: '#0088ff'}}/>
								<MenuOption value={2}>
									<Text style={{color: 'blue'}}>Two</Text>
								</MenuOption>
								<MenuOption value={3} disabled={true} text='Three' />
							</View>
						</MenuOptions> */}
          </Menu>
        )}
      </View>
      <Text style={styles.itemDescription}>{post.description}</Text>
      <View style={{ marginVertical: 15, alignItems: 'center' }}>
        <Carousel
          inactiveSlideOpacity={0.3}
          inactiveSlideScale={0.34}
          data={post.path_img}
          renderItem={({ item }) => (<CarouselItem image={item} height={viewportWidth} width={itemWidth} />)}
          sliderWidth={viewportWidth}
          itemWidth={itemWidth}
          onSnapToItem={(index) => setCarouselIndex(index)}

        />
        <Pagination
          dotsLength={post.path_img.length}
          activeDotIndex={carouselIndex}
          dotColor={colors.primaryDark}
          inactiveDotColor={'gray'}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
      </View>
      <View style={styles.itemIteration}>
        <View style={styles.itemIterationButton}>
          <Text style={styles.itemIterationText}>{reacts[post.id_post].like}</Text>
          <TouchableOpacity onPress={() => createReact(post.id_post, 1, reacts, user, updateFeed, index)}>
            <FontAwesomeIcon name="thumbs-up" size={iconSize} color={(reacts[post.id_post].type === 1 ? colors.primaryDark : 'gray')} />
          </TouchableOpacity>
        </View>
        <View style={styles.itemIterationButton}>
          <Text style={styles.itemIterationText}>{reacts[post.id_post].dislike}</Text>
          <TouchableOpacity onPress={() => createReact(post.id_post, 2, reacts, user, updateFeed, index)}>
            <FontAwesomeIcon name="thumbs-down" size={iconSize} color={(reacts[post.id_post].type === 2 ? colors.primaryDark : 'gray')} />
          </TouchableOpacity>
        </View>
        <View style={styles.itemIterationButton}>
          <Text style={styles.itemIterationText}>{post.comments.length}</Text>
          <TouchableOpacity onPress={() => { setIdPost(post.id_post); toogleModal() }}>
            <FontAwesomeIcon name="comment-alt" size={iconSize} color={'gray'} />
          </TouchableOpacity>
        </View>
        <Text style={styles.itemIterationText}>{time}</Text>
      </View>
    </View>
  )
}

function ItemComment ({ item }) {

  let time = getTimeSend(item.createdAt)

  return (
    <View style={styles.commentsContainer}>
      <View style={styles.commentsContainerTitle}>
        <View style={styles.commentsContainerTitleUser}>
          <Image source={{ uri: api.defaults.baseURL + item.users.path_img }} style={styles.commentsContainerTitleUserImg} />
          <Text style={styles.commentsContainerTitleUserName}>{item.users.name}</Text>
        </View>
        <Text>{time}</Text>
      </View>
      <Text style={styles.commentsContainerText}>{item.text}</Text>
    </View>
  )
}


function CarouselItem ({ image, width, height }) {
  return (
    (image) ?
      <View>
        <Image style={{ height: height, width: width, resizeMode: 'contain' }} source={{ uri: api.defaults.baseURL + image }} />
      </View>
      : <></>
  )
}

async function createReact (id_post, id_react, reacts, user, updateFeed, index) {
  if (Object.keys(user).length === 0) {
    showMessage({
      type: 'danger',
      message: 'Você não está logado'
    })
    return;
  }
  if (id_react === reacts[id_post].type) {
    reacts[id_post].type = 0;
    api.delete('/users/' + user.id_user + "/reacts", {
      data: {
        id: id_post,
        target: 'post',
      }
    }).then(res => {
      if (res.data.status === 'ok') {
        updateFeed(index, id_post)
      } else {
        showMessage({
          type: 'danger',
          message: 'Um erro ocorreu',
          description: 'Provavelmente esse post foi apagado'
        })
        updateFeed(index, id_post, true)
      }
    })
  } else {
    reacts[id_post].type = id_react;
    api.post('/users/' + user.id_user + "/reacts", {
      id: id_post,
      target: 'post',
      type_react: id_react
    }).then(res => {
      if (res.data.status === 'ok') {
        updateFeed(index, id_post)
      } else {
        updateFeed(index, id_post, true)
        showMessage({
          type: 'danger',
          message: 'Um erro ocorreu',
          description: 'Provavelmente esse post foi apagado'
        })
      }
    })
  }


}



async function deletePost (id_post, user, index, updateFeed) {
  api.delete('/users/' + user.id_user + "/posts/" + id_post).then(res => {
    updateFeed(index, id_post, true)
  })
}

async function createComment (id_user, id_post, text) {
  api.post('/users/' + id_user + '/posts/' + id_post + '/comments', {
    text
  }).then(res => {
    if (res.data.status === 'ok') {
      showMessage({
        type: 'success',
        message: 'Comentario criado'
      })
    } else {
      showMessage({
        type: 'danger',
        message: 'Algun erro ocorreu'
      })
    }
  })
}


function getTimeSend (timeCreated) {
  let time = '';
  let dat = [{
    str: 'seconds',
    limit: 60,
    text: 'segundos'
  }, {
    str: 'minutes',
    limit: 60,
    text: 'minutos'
  }, {
    str: 'hours',
    limit: 24,
    text: 'horas'
  }, {
    str: 'days',
    limit: 7,
    text: 'dias'
  }, {
    str: 'weeks',
    limit: 5,
    text: 'semanas'
  }]

  dat.map(d => {
    let ttemp = moment().diff(moment(new Date(timeCreated)), d.str)
    if (ttemp && ttemp <= d.limit) {
      if (ttemp === 1) d.text = d.text.substring(0, d.text.length - 1);
      time = 'Há ' + ttemp + ' ' + d.text;
    }

  })

  return time
}