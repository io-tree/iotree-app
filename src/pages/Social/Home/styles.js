import colors from '../../../globalStyles/colors'
import fonts from '../../../globalStyles/fonts'

import { Dimensions, StyleSheet } from 'react-native'
let { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
  	container: {
		flex: 1,
		justifyContent: 'center'
	},

 	item: {
		borderRadius: 20,
		borderColor: '#ddd',
		borderWidth: 2,
		marginVertical: 8,
		padding: 15,
		margin: 20,
	},

	itemTitle: {
		fontSize: 18,
		marginBottom: 5
	},

	itemDescription: {
		fontSize: 14,
		marginBottom: 5
	},

	itemPhotos: {
		marginBottom: 5
	},

	itemIteration: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around'
	},

	itemIterationButton: {
		flex: 1,
		flexDirection: 'row',
		alignItems: "center"
	},

	itemIterationText: {
		fontSize: 15,
		marginHorizontal: 10
	},

	itemMenu: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center'
	},

	itemOptions: {
		padding: 10,
		paddingHorizontal: 25,
		textAlign: "center",
	},

	comments: {
		height: 50, 
		width: 'auto', 
		borderBottomWidth: 2,
		alignItems: 'center',
		borderColor: colors.primaryDark,
		flexDirection: "row"
	},

	commentsBack: {
		height: 50,
		width: 50,
		alignItems: 'center',
		justifyContent: 'center'

	},

	commentsTitle: {
		flex: 1,
		textAlign: "center",
		color: colors.primaryDark,
		fontSize: 25,
		marginRight: 50
	},

	commentsSendContainer: {
		height: 50,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 15,
		borderTopWidth: 2,
		borderColor: colors.primaryDark,
		alignItems: 'center'
	},

	commentsSendContainerInput: {
		height: 30,
		flex: 1,
		marginRight: 15,
		fontSize: 17,
		padding: 0,
		borderBottomWidth: 1,
		borderColor: colors.primary,
	},

	commentsContainer: {
		borderRadius: 5,
		borderWidth: 1,
		padding: 5,
		marginBottom: 10,
		borderColor: 'gray'
	},

	commentsContainerText: {
		padding: 10,
		
	},

	commentsContainerTitle: {
		height: 40,
		flexDirection: 'row', 
		justifyContent: 'space-between',
		alignItems: 'center',
		borderBottomWidth: 1
	},

	commentsContainerTitleUser: {
		flexDirection: 'row',
		alignItems: 'center'
	},

	commentsContainerTitleUserName: {
		fontSize: 15
	},

	commentsContainerTitleUserImg: {
		height: 25, 
		width: 25, 
		resizeMode: 'contain',
		marginHorizontal: 5
	}


});
export default styles;