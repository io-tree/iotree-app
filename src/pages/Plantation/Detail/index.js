import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  Image
} from 'react-native';
import moment from 'moment';
import ProgressBar from 'react-native-progress/Bar';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { showMessage } from 'react-native-flash-message';

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import EntypoIcon from 'react-native-vector-icons/Entypo';

import styles from './styles';
import colors from '../../../globalStyles/colors';
import api from '../../../services/api';
import socket from '../../../services/socket';

import ViewAuthorization from '../../../components/ViewAuthorization';
import AsyncStorage from '@react-native-community/async-storage';
import LoadingScreen from '../../../components/LoadingScreen';
import ModalAction from '../../../components/ModalAction';

const iconSizeFontAwesome = 35;
const iconSizeMaterial = 45;

export default function PlantationDetail ({ navigation }) {

  let didWillFocus = null;

  const [plantation, setPlantation] = useState({});
  const [valueProgress, setValueProgress] = useState(0);
  const [nameGreenhouse, setNameGreenhouse] = useState('');
  const [temperature, setTemperature] = useState(0);
  const [humidityAir, setHumidityAir] = useState(0);
  const [humidityGround, setHumidityGround] = useState(0);

  const [loading, setLoading] = useState(true);
  const [enabled, setEnabled] = useState(false);
  const [enabledModal, setEnabledModal] = useState(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      await init();
      setLoading(false);
    })();

    didWillFocus = navigation.addListener('willFocus', async () => {
      setLoading(true);
      await init();
      setLoading(false);
    });

  }, []);

  useEffect(() => () => {
    didWillFocus.remove();
    socket.off('sensor');
  }, [])

  async function init () {
    const id_user = await getUser();
    if (id_user) {
      let plantation = await api.get(`/users/${id_user}/plantations/${getPlantation()}`);
      plantation = plantation.data.body[0];
      setPlantation(plantation);
      setNameGreenhouse(plantation.greenhouses.name);

      setTemperature(plantation.temperature);
      setHumidityAir(plantation.humidity_air);
      setHumidityGround(plantation.humidity_ground);

      let total = moment(new Date(plantation.done_time)).diff(new Date(plantation.createdAt), 'day');
      let i = moment(new Date()).diff(new Date(plantation.createdAt), 'days');
      let f = moment(new Date(plantation.done_time)).diff(new Date(), 'days');
      setValueProgress(((100 * i) / total) / 100)
    }

    socket.on('sensor', (data) => {
      console.log(data)
      if (plantation) {
        if (getPlantation() === data.id_plantation) {
          setTemperature(data.temperature);
          setHumidityAir(data.humidity_air);
          setHumidityGround(data.humidity_ground);
        }
      }
    });
  }

  async function getUser () {
    const user = await AsyncStorage.getItem('user');
    if (user) {
      return (JSON.parse(user)).id_user
    }
    return false
  }

  function getPlantation () {
    return navigation.getParam('id_plantation')
  }

  async function updateNameGreenhouse () {
    const id_user = await getUser();
    const res = await api.put(`/users/${id_user}/greenhouses/${plantation.id_greenhouse}`, {
      name: nameGreenhouse,
      confirm_password: false
    });

    if (res.data.status === 'ok') {
      showMessage({
        type: 'success',
        message: res.data.msg,
        autoHide: true,
        duration: 2000
      })
    }
  }

  async function deletePlantation () {
    const id_user = await getUser();
    const res = await api.delete(`/users/${id_user}/plantations/${getPlantation()}`);

    if (res.data.status === 'ok') {
      showMessage({
        type: 'success',
        message: res.data.msg,
        autoHide: true,
        duration: 2000
      })
      navigation.goBack();
    }
  }

  return (
    <ViewAuthorization
      navigation={navigation}
      headerRight={{
        iconName: 'trash',
        iconColor: 'red',
        iconSize: 20,
        function: () => setEnabledModal(true)
      }}
    >
      <LoadingScreen enabled={loading} />
      {!loading &&
        <ScrollView>
          <View style={styles.container}>
            <View style={styles.containerNameGreenhouse}>
              <TextInput
                selectTextOnFocus={true}
                editable={true}
                autoFocus={enabled}
                style={styles.greenhouseName}
                onChangeText={text => setNameGreenhouse(text)}
                onBlur={() => {
                  setEnabled(false);
                  updateNameGreenhouse()
                }}
                onFocus={() => setEnabled(true)}
                value={nameGreenhouse}
              />
              <FontAwesomeIcon name={'pen'} size={20} color={colors.primary} />
            </View>
            <Image
              style={styles.plantImage}
              source={{ uri: api.defaults.baseURL + plantation.plants.path_img }}
            />
            <View style={styles.boxDetail}>
              <BoxDetail
                icon={<FontAwesomeIcon name={'cloud-rain'} color={colors.primary} size={iconSizeFontAwesome} />}
                name={`${humidityAir}%`}
              />
              <BoxDetail
                icon={<FontAwesomeIcon name={'temperature-high'} color={colors.primary} size={iconSizeFontAwesome} />}
                name={`${temperature}°C`}
              />
              <BoxDetail
                icon={<MaterialCommunityIcon name={'water-percent'} color={colors.primary} size={iconSizeMaterial} />}
                name={`${humidityGround}%`}
              />
              <BoxDetail
                icon={<EntypoIcon name={'water'} color={colors.primary} size={iconSizeFontAwesome} />}
                name={`Irrigação em ${moment(new Date(plantation.time_irrigation)).diff(new Date(), 'hours')} horas`}
              />
              <BoxDetail
                icon={<MaterialCommunityIcon name={'restore-clock'} color={colors.primary} size={iconSizeMaterial} />}
                name={`Plantado há ${moment(new Date()).diff(new Date(plantation.createdAt), 'days')} dias`}
              />
              <BoxDetail
                icon={<MaterialCommunityIcon name={'update'} color={colors.primary} size={iconSizeMaterial} />}
                name={`Colheita ${moment(new Date(plantation.done_time)).diff(new Date(), 'days')} dias`}
              />
            </View>
            <View style={styles.containerProgress}>
              <View style={styles.containerProgressIcon}>
                <FontAwesomeIcon name={'seedling'} color={colors.primary} size={20} />
                <Text style={styles.containerProgressIconText}>Plantado {moment(new Date(plantation.createdAt)).format('DD/MM/YYYY')}</Text>
              </View>
              <ProgressBar
                progress={valueProgress}
                width={200}
                height={10}
                color={colors.primary}
                borderColor={colors.grayDark}
              />
              <View style={styles.containerProgressIcon}>
                <EntypoIcon name={'tree'} color={colors.primary} size={20} />
                <Text style={styles.containerProgressIconText}>Plantado {moment(new Date(plantation.done_time)).format('DD/MM/YYYY')}</Text>
              </View>
            </View>
          </View>
          <ModalAction
            enabled={enabledModal}
            onClose={() => setEnabledModal(false)}
            listAction={[
              {
                label: 'Deletar',
                function: () => deletePlantation()
              },
              {
                label: 'Cancelar'
              }
            ]}
          >
            <Text>Deseja apagar realmente?</Text>
          </ModalAction>
        </ScrollView>
      }
    </ViewAuthorization>
  )
}

function BoxDetail ({ icon, name }) {
  return (
    <View style={styles.boxDetailItem}>
      {icon}
      <Text style={styles.boxDetailItemText}>{name}</Text>
    </View>
  )
}