import { 
	StyleSheet, 
} from 'react-native';
import colors from '../../../globalStyles/colors';

const boxSize = 200;
const boxSizeDetail = 95;

export default StyleSheet.create({

	container: {
		justifyContent: 'center',
		alignItems: 'center'
	},

	containerNameGreenhouse: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: "center",
		marginBottom: 15
	},

	greenhouseName: {
		paddingHorizontal: 10,
		fontSize: 16
	},

	plantImage: {
		height: 250,
		width: 250,
		borderRadius: 10,
	},

	boxDetail: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		flexWrap: 'wrap',
		padding: 5,
		marginTop: 20
	},

	boxDetailItem: {
		padding: 5,
		margin: 10,
		width: boxSizeDetail,
		height: boxSizeDetail,
		justifyContent: 'space-evenly',
		alignItems: 'center',
		borderRadius: 8,
		borderWidth: 1,
		borderColor: colors.gray,
		backgroundColor: 'white',
		elevation: 3
	},

	boxDetailItemText: {
		marginTop: 5, 
		fontSize: 12, 
		textAlign: 'center'
	},

	boxDescription: {
		fontSize: 15,
		marginTop: 15,
		textAlign: 'justify'
	},

	containerProgress: {
		flexDirection: 'row',
		width: '100%',
		justifyContent: 'space-around',
		alignItems: 'center'
	},

	containerProgressIcon: {
		justifyContent: 'center',
		alignItems: 'center'
	},

	containerProgressIconText: {
		fontSize: 10,
		color: colors.primaryDark,
		width: 80,
		textAlign: 'center'
	}
}) 