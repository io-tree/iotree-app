import { 
	StyleSheet, 
} from 'react-native';

import colors from '../../../globalStyles/colors';


const widthButton = 80;

export default StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		alignSelf: 'stretch',
		paddingHorizontal: 35,
		paddingVertical: 20,
		justifyContent: 'space-around'
	},

	box: {
		width: '100%',
	},

	text: {
		fontSize: 17,
		color: colors.headerText,
		fontFamily:'Montserrat-Regular',
		textAlign: 'center'
	},

	image: {
		width: widthButton,
		height: widthButton
	},

	button: {
		width: widthButton,
		height: widthButton - 30,
		backgroundColor: 'white',
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 5,
		elevation: 3,
		marginTop: 25,
		overflow: 'hidden',
		alignSelf: 'center'
	},

	buttonOk: {
		backgroundColor: colors.primaryDark,
		paddingHorizontal: 10,
		paddingVertical: 15,
		borderRadius: 5,
		marginTop: 18,
		elevation: 3,
		alignItems: 'center',
		alignSelf: 'center'
	},
}) 