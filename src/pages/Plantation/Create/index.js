import React, { useState, useEffect } from 'react';
import { 
	View, 
	Text, 
	Image, 
	TextInput, 
	TouchableOpacity 
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { showMessage } from "react-native-flash-message";
import AsyncStorage from '@react-native-community/async-storage';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';

import styles from './styles';
import colors from '../../../globalStyles/colors';
import api from '../../../services/api';

import LoadingScreen from '../../../components/LoadingScreen';
import GeneralSelect from '../../../components/GeneralSelect';

export default function CreatePlantation({ navigation }) {
	
	let didWillMount = null;

	const [ plant, setPlant ] = useState({});
	const [ idGreenhouse, setIdGrenhouse ] = useState(0);
	const [ user, setUser ] = useState({});
	const [ loading, setLoading ] = useState(false);

	useEffect(() => {
		(async () => {
			await getUser();
		})();

		if(!navigation.getParam('reset'))
			(async () => await getPlant())();
		
		
		didWillMount = navigation.addListener('willFocus', async () => {
			setLoading(true);
			await getPlant();
			await getUser();
			setLoading(false);
		})
	},[]);
	
	useEffect(() => () => {
		didWillMount.remove();
	}, []);
	
	async function getPlant() {
		let id_plant = navigation.dangerouslyGetParent().getParam('id_plant');
		if(id_plant){
			navigation.setParams({id_plant: undefined})
			let plant = await api.get(`/plants?plant=${id_plant}`);
	
			setPlant(plant.data.body[0]);
		}
	}

	async function getUser() {
		const user = JSON.parse(await AsyncStorage.getItem('user'));
		setUser(user);
		return user.id_user;
	}

	function setPlantation() {
		setLoading(true);
		api.post(`/users/${user.id_user}/plantations`, {
			id_plant:plant.id_plant,
			id_greenhouse: idGreenhouse
		}).then( res => {
			if(res.data.status === 'ok') {
				navigation.goBack();
				showMessage({
					type: 'success',
					message: res.data.msg
				})
			} else {
				showMessage({
					type: 'danger',
					message: (res.data.msg === 'validationError' ? res.data.body.join('\n') : res.data.msg)
				})
			}
			setLoading(false)
		})
	}

	{/* <Image 
		style={styles.image}
		source={{uri: api.defaults.baseURL + plant.path_img}}
	/> */}

	return (
		<View style={styles.container}>
			<LoadingScreen enabled={loading}/>
			<View style={styles.box}>
				<Text style={styles.text}>Escolha qual planta você deseja plantar</Text>
				<TouchableOpacity 
					onPress={() => navigation.navigate('Plant', null, NavigationActions.navigate({ routeName: 'Category' }))}
					style={styles.button}
				>
					{Object.keys(plant).length > 0 ?
						<FontAwesomeIcon name={'check'} color={colors.primary} size={25} />
					:
						<FontAwesomeIcon name={'book'} color={colors.primary} size={25} />
					}
				</TouchableOpacity>
			</View>

			<View style={styles.box}>
				<Text style={styles.text}>Selecione seu equipamento</Text>
				<GeneralSelect 
					model='greenhouse'
					idSelected={idGreenhouse}
					setIdSelected={setIdGrenhouse}
				/>
			</View>

			<View style={styles.box}>
				<Text style={styles.text}>Após plantar a semente, clique no botão a seguir</Text>
				<TouchableOpacity 
					onPress={() => setPlantation()}
					style={styles.buttonOk}
					disabled={loading}
				>
					<Text style={{color: 'white', fontWeight: 'bold', fontSize: 16}}>Plantar</Text>
				</TouchableOpacity>
			</View>
		</View>
	)
}
