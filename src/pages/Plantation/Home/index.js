import React, { useState, useEffect } from 'react';
import { 
	View, 
	Text, 
	Image, 
	TextInput, 
	TouchableOpacity 
} from 'react-native';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

//import ActionButton from 'react-native-action-button';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';
import colors from '../../../globalStyles/colors';
import api from '../../../services/api';

import LoadingScreen from '../../../components/LoadingScreen';
import ScrollViewRefresh from '../../../components/ScrollViewRefresh';
import ViewAuthorization from '../../../components/ViewAuthorization';

export default function Home({ navigation }) {

	let didWillFocus = null;

	const [ listPlantations, setListPlantations ] = useState([]);
	const [ idUser, setIdUser ] = useState(null);
	const [ loading, setLoading ] = useState(true);

	useEffect(() => {

		(async () => {
			await getPlantations();
			setLoading(false);
		})();

		didWillFocus = navigation.addListener('willFocus', async () => {
			await getPlantations();
			setLoading(false);
		});
	}, []);

	useEffect(() => () => {
		didWillFocus.remove();
	},[]);

	async function getPlantations(){
		const id_user = await getUser();
		if(id_user){
			const plantations = await api.get(`/users/${id_user}/plantations`);
			setListPlantations(plantations.data.body);
		}
	}

	async function getUser(){
		const user = await AsyncStorage.getItem('user');
		if(user){
			const id_user = JSON.parse(user).id_user
			setIdUser(id_user)
			return id_user;
		}
		return false
	}

	return (
		<ViewAuthorization
			navigation={navigation}
			headerRight={{
				iconName: 'plus',
				function: () => navigation.navigate('Create', {reset: true})
			}}
		>
			<LoadingScreen enabled={loading} />
			{listPlantations.length > 0 ?
				<ScrollViewRefresh
					onRefresh={
						() => getPlantations()
					}
				>
					{!loading &&
						listPlantations.map( (plantation, index) => {
		
							let day = moment(new Date(plantation.done_time)).diff(new Date(), 'day');
		
							return (
								<TouchableOpacity 
									key={index} 
									style={styles.boxPlantation} 
									onPress={() => navigation.navigate('Detail', {
										title: plantation.plants.name,
										id_plantation: plantation.id_plantation
									})}
								>
									<Image 
										style={styles.boxImage}
										source={{
											uri: api.defaults.baseURL + plantation.plants.path_img
										}}
									/>
									<View style={styles.boxStatus}>
										<Text style={styles.boxTextPlant}>{plantation.plants.name}</Text>
										<Text style={styles.boxTextGreenhouse}>{plantation.greenhouses.name}</Text>
									</View>
									<View style={styles.boxTime}>
										<MaterialCommunityIcon name={'clock-outline'} color={colors.primaryDark} size={20} />
										<Text style={styles.boxTextTime}>{day} dias</Text>
									</View>
								</TouchableOpacity>
							)
						})
					}
				</ScrollViewRefresh>
			:
				<View style={styles.containerPreview}>
					<Text>Você não possui plantas</Text>
				</View>
			}
		</ViewAuthorization>
	)
}
