import React, { useState, useEffect } from 'react';
import { 
	View, 
	Text, 
	TouchableOpacity 
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { TextField } from 'react-native-material-textfield';
import { showMessage } from 'react-native-flash-message';

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';
import colors from '../../../globalStyles/colors';
import api from '../../../services/api';

import LoadingScreen from '../../../components/LoadingScreen';
import ScrollViewRefresh from '../../../components/ScrollViewRefresh';
import BottomSheet from '../../../components/BottomSheet';
import ModalAction from '../../../components/ModalAction';
import ViewAuthorization from '../../../components/ViewAuthorization';

export default function Home({ navigation }) {

	let didWillFocus = null;

	const [ listGreenhouses, setListGreenhouses ] = useState([]);
	const [ idGreenhouse, setIdGrenhouse ] = useState('');
	const [ password, setPassword ] = useState(null);
	const [ idUser, setIdUser ] = useState(null);

	const [ loading, setLoading ] = useState(true);
	const [ enabledModal, setEnabledModal ] = useState(false);
	const [ openBottomSheet, setOpenBottomSheet ] = useState(false);

	useEffect(() => {
		
		(async () => {
			await init();
		})();

		didWillFocus = navigation.addListener('willFocus', async () => {
			await init();
		});
	}, []);

	async function init(){
		await getGreenhouses();
		setLoading(false);
	}

	useEffect(() => () => {
		didWillFocus.remove();
	},[]);

	useEffect(() => {
		if(idGreenhouse.length >= 16){
			setEnabledModal(true);
		}
	}, [idGreenhouse])


	async function addGreenhouse() {
		setLoading(true);
		const id_user = await getUser();
		api.post(`/users/${id_user}/greenhouses/${idGreenhouse}`, {
			password,
			link: true
		}).then( async res => {
			if(res.data.status === 'ok') {
				showMessage({
					type: 'success',
					message: res.data.msg
				})

				await init()
			} else {
				showMessage({
					type: 'danger',
					message: (res.data.msg === 'validationError' ? res.data.body.join('\n') : res.data.msg)
				})
			}

			setIdGrenhouse('');
			setPassword('');
			setLoading(false);
		})
	}

	async function getGreenhouses(){
		const id_user = await getUser();
		if(id_user){
			const greenhouses = await api.get(`/users/${id_user}/greenhouses`);
			setListGreenhouses(greenhouses.data.body);
		}
	}

	async function getUser(){
		const user = await AsyncStorage.getItem('user');
		if(user){
			const id_user = JSON.parse(user).id_user
			setIdUser(id_user)
			return id_user;
		}
		return false
	}

	return (
		<ViewAuthorization 
			navigation={navigation}
			headerRight={{
				iconName: 'plus',
				function: () => setOpenBottomSheet(true)
			}}
		>
			<LoadingScreen enabled={loading} />
			{listGreenhouses.length > 0?
				<ScrollViewRefresh
					onRefresh={init}
				>
					{!loading &&
						listGreenhouses.map( (greenhouse, index) => {
							return (
								<TouchableOpacity 
									key={index} 
									style={styles.boxGreenhouse}
									onPress={() => navigation.navigate('Edit', { idGreenhouse: greenhouse.id_greenhouse})}
								>
									<View style={styles.boxStatus}>
										<Text style={styles.boxTextName}>{greenhouse.name}</Text>
										<Text style={styles.boxTextType}>{greenhouse.greenhouse_type.name}</Text>
									</View>
									<View style={styles.boxTime}>
										<MaterialCommunityIcon name={(greenhouse.online ? "check-circle" : "stop-circle")} color={(greenhouse.online ? colors.primaryDark : 'red')} size={15} />
										<Text style={{...styles.boxTextTime, color: (greenhouse.online ? colors.primaryDark : 'red')}}>{(greenhouse.online ? "online" : "offline")}</Text>
									</View>
								</TouchableOpacity>
							)
						})
					}
				</ScrollViewRefresh>
			:
				<View style={styles.containerPreview}>
					<Text>Você não possui estufas cadastradas</Text>
				</View>
			}
			<BottomSheet 
				enabled={openBottomSheet}
				onClose={() => setOpenBottomSheet(false)}
				listAction={[
					{
						icon: 'camera',
						label: 'Ler QR Code',
						function: () => {
							 navigation.navigate('Scanner', { onScanner: setIdGrenhouse})
						}
					},
					{
						icon: 'pen',
						label: 'Digitar o codigo',
						function: () => setEnabledModal(true)
					}
				]}
			/>
			<ModalAction
				enabled={enabledModal}
				onClose={() => setEnabledModal(false)}
				listAction={[
					{
						label: 'Adicionar',
						function: () => {
							addGreenhouse()
						}
					},
					{
						label: 'Cancelar',
						function: () => {
							setIdGrenhouse('');
							setPassword('');
						}
					}
				]}
			>
				<TextField
					label='Codigo'
					title='Digite o código da estufa'
					tintColor={colors.primaryDark}
					autoCapitalize="none"
					autoCorrect={false}
					autoCompleteType="off"
					value={idGreenhouse}
					nextFocusDown={true}
					onChangeText={setIdGrenhouse}
					style={styles.textFieldText}
					titleTextStyle={styles.textFieldText}
					labelTextStyle={styles.textFieldText}
					maxLength={16}
					placeholder={'DFL-4f7b2n8e1g82'}
				/>
				<TextField
					label='Senha'
					title='Digite a senha da estufa'
					tintColor={colors.primaryDark}
					secureTextEntry={true}
					autoCapitalize="none"
					autoCorrect={false}
					autoCompleteType="off"
					value={password}
					nextFocusDown={true}
					onChangeText={setPassword}
					style={styles.textFieldText}
					titleTextStyle={styles.textFieldText}
					labelTextStyle={styles.textFieldText}
				/>
			</ModalAction>

		</ViewAuthorization>
	)
}
