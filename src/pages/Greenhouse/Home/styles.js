import { 
	StyleSheet, 
} from 'react-native';
import colors from '../../../globalStyles/colors';
import fonts from '../../../globalStyles/fonts';

export default StyleSheet.create({
	container: {
		flex: 1,
		alignSelf: 'stretch',
		paddingHorizontal: 35,
		paddingTop: 40
	},

	containerPreview: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},

	boxGreenhouse: {
		height: 80,
		width: '100%',
		flexDirection: 'row',
		overflow: 'hidden',
		borderRadius: 10,
		backgroundColor: 'white',
		//elevation: 2,
		marginBottom: 15,
		borderColor: colors.gray,
		borderWidth: 2
	},

	boxImage: {
		height: 100,
		width: 100,
		alignSelf: "flex-start"
	},

	boxStatus: {
		marginHorizontal: 15,
		flexDirection: 'column',
		justifyContent: 'space-evenly',
		alignItems: 'flex-start',
		flex: 1
	},

	boxTextName: {
		fontSize: 16,
		color: colors.primaryDark,
		fontWeight: 'bold',
	},

	boxTextType: {
		fontSize: 15
	},

	boxTime: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'flex-end',
		padding: 10
	},

	boxTextTime: {
		fontSize: 14,
		marginHorizontal: 3,
		fontStyle: 'italic',
		fontWeight: 'bold'
	},

	textFieldText:{
		fontFamily:fonts.medium,
		color:colors.black
	},
}) 