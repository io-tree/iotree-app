import React, { useState, useEffect } from 'react';
import { 
	View, 
	Text, 
	TouchableOpacity 
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { TextField } from 'react-native-material-textfield';
import { showMessage } from 'react-native-flash-message';

import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles';
import colors from '../../../globalStyles/colors';
import api from '../../../services/api';

import LoadingScreen from '../../../components/LoadingScreen';
import ScrollViewRefresh from '../../../components/ScrollViewRefresh';
import ButtonDefault from '../../../components/ButtonDefault';
import ModalAction from '../../../components/ModalAction';
import ViewAuthorization from '../../../components/ViewAuthorization';

export default function Edit({ navigation }) {

	let didWillFocus = null;

	const [ idUser, setIdUser ] = useState(null);
	const [ name, setName ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ newPassword, setNewPassword ] = useState('');

	const [ loading, setLoading ] = useState(true);
	const [ enabledModal, setEnabledModal ] = useState(false);

	useEffect(() => {
		
		(async () => {
			await init();
		})();

		didWillFocus = navigation.addListener('willFocus', async () => {
			await init();
		});
	}, []);

	async function init(){
		await getGreenhouse();
		setLoading(false);
	}

	useEffect(() => () => {
		didWillFocus.remove();
	},[]);

	function getIdGreenhouse(){
		return navigation.getParam('idGreenhouse');
	}


	async function updateGreenhouse() {
		setLoading(true);
		const id_user = await getUser();
		api.put(`/users/${id_user}/greenhouses/${getIdGreenhouse()}`, {
			name,
			password,
			new_password: (newPassword.length > 0 ? newPassword : undefined),
			confirm_password: true
		}).then( async res => {
			if(res.data.status === 'ok') {
				showMessage({
					type: 'success',
					message: res.data.msg
				})

				await init()
			} else {
				showMessage({
					type: 'danger',
					message: (res.data.msg === 'validationError' ? res.data.body.join('\n') : res.data.msg)
				})
			}

			setPassword('');
			setLoading(false);
		})
	}

	async function getGreenhouse(){
		const id_user = await getUser();
		if(id_user){
			let greenhouse = await api.get(`/users/${id_user}/greenhouses/${getIdGreenhouse()}`);
			if(greenhouse){
				greenhouse = greenhouse.data.body[0];

				setName(greenhouse.name)
			}
		}
	}

	async function getUser(){
		const user = await AsyncStorage.getItem('user');
		if(user){
			const id_user = JSON.parse(user).id_user
			setIdUser(id_user)
			return id_user;
		}
		return false
	}

	return (
		<ViewAuthorization 
			navigation={navigation}
		>
			<LoadingScreen enabled={loading} />
			{!loading &&
				<ScrollViewRefresh
					onRefresh={init}
				>
					<TextField
						label='Nome'
						title='Digite o nome da estufa'
						tintColor={colors.primaryDark}
						secureTextEntry={false}
						autoCapitalize="none"
						autoCorrect={false}
						autoCompleteType="off"
						value={name}
						nextFocusDown={true}
						onChangeText={setName}
						style={styles.textFieldText}
						titleTextStyle={styles.textFieldText}
						labelTextStyle={styles.textFieldText}
					/>
					<TextField
						label='Nova senha'
						title='Digite uma nova senha da estufa'
						tintColor={colors.primaryDark}
						secureTextEntry={true}
						autoCapitalize="none"
						autoCorrect={false}
						autoCompleteType="off"
						value={newPassword}
						nextFocusDown={true}
						onChangeText={setNewPassword}
						style={styles.textFieldText}
						titleTextStyle={styles.textFieldText}
						labelTextStyle={styles.textFieldText}
					/>
					<ButtonDefault buttonLabel={'Salvar'} onSubmit={ () => setEnabledModal(true)} />
				</ScrollViewRefresh>
			}
			<ModalAction
				enabled={enabledModal}
				onClose={() => setEnabledModal(false)}
				listAction={[
					{
						label: 'Confirmar',
						function: () => {
							updateGreenhouse()
						}
					},
					{
						label: 'Cancelar',
					}
				]}
			>
				<TextField
					label='Senha'
					title='Digite a senha da estufa'
					tintColor={colors.primaryDark}
					secureTextEntry={true}
					autoCapitalize="none"
					autoCorrect={false}
					autoCompleteType="off"
					value={password}
					nextFocusDown={true}
					onChangeText={setPassword}
					style={styles.textFieldText}
					titleTextStyle={styles.textFieldText}
					labelTextStyle={styles.textFieldText}
				/>
			</ModalAction>

		</ViewAuthorization>
	)
}
