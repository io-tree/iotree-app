import React, { useState, useEffect } from 'react';
import { 
	View, 
	Text, 
	StyleSheet, 
	TextInput, 
	TouchableOpacity,
	Dimensions
} from 'react-native';

//import ActionButton from 'react-native-action-button';
import QRCodeScanner from 'react-native-qrcode-scanner';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';

import styles from './styles';
import colors from '../../../globalStyles/colors';
import api from '../../../services/api';

const { height, width } = Dimensions.get('window');

const boxScanner = 230;
const top = 80;

export default function Home({ navigation }) {
	
	function onSuccess(e){
		//console.log(e.data)
		navigation.goBack()
		navigation.state.params.onScanner(e.data)
	}

	return (
		<QRCodeScanner
			onRead={onSuccess}
			showMarker
			topViewStyle={{
				flex: 0,
				height: top,
			}}
			bottomViewStyle={{
				flex: 0,
			}}
			topContent={
				<Text style={styles.centerText}>
					Aponte a câmera para o QR Code do seu produto
				</Text>
			}
			cameraStyle={{
				height: height - 130 - top,
				overflow: 'hidden'
			}}
			customMarker={
				<View 
					style={{
						height: '100%',
						width: '100%',
						justifyContent: 'center',
						alignItems: 'center',
						borderTopWidth: (height - boxScanner - 130 - top) / 2,
						borderBottomWidth: (height - boxScanner - 130 - top) / 2,
						borderLeftWidth: (width - boxScanner) / 2,
						borderRightWidth: (width - boxScanner) / 2,
						borderColor: 'rgba(0,0,0,0.5)', 
					}}
				>
					<View style={{
						borderColor: colors.primaryLight,
						borderWidth: 7,
						borderRadius: 15,
						width: boxScanner + 10,
						height: boxScanner + 10,
						position: 'absolute'
					}}/>
				</View>
			}
		/>
	)
}
