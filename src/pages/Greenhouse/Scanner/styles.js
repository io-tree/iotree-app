import { 
	StyleSheet, 
} from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignSelf: 'stretch',
	},

	centerText: {
		fontSize: 18,
		color: '#777',
		
	},
	
	textBold: {
		fontWeight: '500',
		color: '#000'
	},

	buttonText: {
		fontSize: 21,
		color: 'rgb(0,122,255)'
	},

	buttonTouchable: {
		padding: 16
	}
}) 