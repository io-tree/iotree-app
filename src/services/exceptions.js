

export default function Exceptions(exception) {
	switch (exception.code) {
		case 'ECONNABORTED': {
			return {
				message: 'Ops... Servidor com demora para responder',
				description: 'Por favor, tente novamente mais tarde',
				type: "warning",
			}
		}
		default: {
			return {
				message: 'Ops... Um problema detectado',
				description: 'Por favor, tente novamente mais tarde',
				type: "danger",
			}
		}
		
	}
}