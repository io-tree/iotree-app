import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const api = axios.create({
  //baseURL: 'http://10.0.0.134:3001/api/v1',
  baseURL: 'https://api.iotree.app/api/v1',
  headers: {
    'Content-Type': 'application/json',
  }
});

api.interceptors.request.use(async request => {

  const token = await AsyncStorage.getItem('token');
  const refreshToken = await AsyncStorage.getItem('refreshToken');

  if (token && refreshToken) {
    request.headers.Authorization = `Bearer ${token}`;
    request.headers['refresh-token'] = refreshToken;
  }
  console.log('Request -> ', request.data);

  return request;
});

api.interceptors.response.use(async response => {
  console.log('Response - > ', response.data);

  if (response.data.token) {
    await AsyncStorage.setItem('token', response.data.token);
    await AsyncStorage.setItem('refreshToken', response.data.refreshToken);
  }

  return response;
})

export default api;