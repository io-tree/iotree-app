import io from 'socket.io-client';
import api from './api';

const socket = io(api.defaults.baseURL.split('/', 3).join("/"), { forceNode: true });

export default socket;