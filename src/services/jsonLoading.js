export const loads =  [ // -> [ file, speed, width ]
	[require('../assets/loading_1.json'), 1.4, 400],
	[require('../assets/loading_2.json'), 0.8, 220],
]
export const loadSelected = Math.floor((Math.random() * loads.length))