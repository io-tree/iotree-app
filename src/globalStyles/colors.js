export default {
  primaryLight: '#6AFFA6',
  primary: '#00E676',
  primaryDark: '#00B248',

  textColor: '#FAFAFA',

  gray: '#CECECE',
  grayDark:'#A7A0A0',
  
  headerText: '#707070',
  background: '#FAFAFA',
}