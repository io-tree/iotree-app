export default {
  //tipo de fonte
  regular:'Montserrat-Regular',
  medium: 'Montserrat-Medium',
  italic:'Montserrat-Italic',
  bold:'Montserrat-Bold',

  //tamanho da fonte
  xxs:10,
  xs:12,
  s:14,
  m:16,
  l:20,
  xl:22,
  xxl:24

}