import React from 'react';
import { Text, View, Dimensions } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator, Header } from 'react-navigation-stack';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome5';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import SplasScreen from './pages/SplashScreen';

import Login from './pages/User/Login';
import EditUser from './pages/User/Edit/';
import CreateUser from './pages/User/Create';
import ChangePassword from './pages/User/ChangePassword';
import RecoverPassword from './pages/User/RecoverPassword';

import Plantation from './pages/Plantation/Home';
import CreatePlantation from './pages/Plantation/Create';
import DetailPlantation from './pages/Plantation/Detail';

import Category from './pages/Plant/Category';
import Plant from './pages/Plant/Plant';
import DetailPlant from "./pages/Plant/DetailPlant";

import Social from './pages/Social/Home';
import CreatePost from './pages/Social/CreatePost';

import Greenhouse from './pages/Greenhouse/Home';
import Scanner from './pages/Greenhouse/Scanner';
import EditGreenhouse from './pages/Greenhouse/Edit';


import colors from './globalStyles/colors';
import { TouchableOpacity } from 'react-native-gesture-handler';

const iconSize = 25;


const { height, width} = Dimensions.get('screen');

const stackStyle = {
	headerStyle:{
		backgroundColor:'transparent',
		elevation:0,
	},
	cardStyle:{
		backgroundColor: colors.background,
	},
	headerRightContainerStyle: {
		marginRight: 15
	},
	headerTitleAlign: "center",
	headerTitleStyle: {
		marginTop: 0,
		borderBottomWidth: 2,
		borderBottomColor: colors.primary,
		textAlign: 'center',
		fontSize: 20,
		width: width - 80,
		paddingVertical: 10,
		color: colors.headerText,
		alignSelf :'center',
		fontFamily:'Montserrat-Regular'
	},
	safeAreaInsets: { //safeAreaInsets
		top: 0, 
		bottom: 0 
	},
	headerTintColor: colors.primary,
	headerBackImage: ({tintColor}) => ( 
		<FontAwesomeIcon name="angle-left"  color={tintColor} size={iconSize} /> 
	),
	headerBackTitleVisible: false
}

const UserStack = createStackNavigator({
	Login: {
		screen: Login,
		navigationOptions: {
			title: 'Conta',
			...stackStyle
		}
	},
	Edit: {
		screen: EditUser,
		navigationOptions: {
			title: 'Editar',
			...stackStyle
		}
	},
	ChangePassword: {
		screen: ChangePassword,
		navigationOptions: ({navigation}) => ({
			title: 'Mudar senha',
			...stackStyle
		})
	},
	Create: {
		screen: CreateUser,
		navigationOptions: {
			title: 'Cadastrar',
			...stackStyle
		}
	},
	RecoverPassword: {
		screen: RecoverPassword,
		navigationOptions: {
			title: 'Redefinir senha',
			...stackStyle
		}
	}
});

const SocialStack = createStackNavigator({
	Social: {
		screen: Social,
		navigationOptions: ({navigation}) => ({
			headerRight: ({ tintColor }) => (
				<TouchableOpacity onPress={()=>navigation.navigate('CreatePost')}>
					<FontAwesomeIcon name="camera"  color={tintColor} size={iconSize} /> 
				</TouchableOpacity>
			),
			title: 'Social',
			...stackStyle
		})
	},
	CreatePost: {
		screen: CreatePost,
		navigationOptions: {
			title: 'Criar Post',
			...stackStyle
		}
	}
})

const PlantStack = createStackNavigator({
	Category: {
		screen: Category,
		navigationOptions: {
			title: 'Categorias de plantas',
			...stackStyle
		}
	},
	Plant: {
		screen: Plant,
		navigationOptions: {
			title: 'Plantas',
			...stackStyle
		}
	},
	DetailPlant: {
		screen: DetailPlant,
		navigationOptions: ({navigation}) => ({
			title: navigation.getParam('title', ''),
			...stackStyle
		})
	}
});

const PlantationStack = createStackNavigator({
	Home: {
		screen: Plantation,
		navigationOptions: ({navigation}) => ({
			title: 'Minhas plantas',
			headerRight: navigation.getParam('headerRight'),
			...stackStyle
		})
	},
	Create: {
		screen: CreatePlantation,
		navigationOptions: {
			title: 'Adicionar plantação',
			...stackStyle
		}
	},
	Detail: {
		screen: DetailPlantation,
		navigationOptions: ({navigation}) => ({
			title: navigation.getParam('title', ''),
			headerRight: navigation.getParam('headerRight'),
			...stackStyle
		})
	}
});

const GreenhouseStack = createStackNavigator({
	Home: {
		screen: Greenhouse,
		navigationOptions: ({navigation}) => ({
			title: 'Minhas estufas',
			headerRight: navigation.getParam('headerRight'),
			...stackStyle
		})
	},
	Scanner: {
		screen: Scanner,
		navigationOptions: {
			title: 'Ler QR Code',
			...stackStyle
		}
	},
	Edit: {
		screen: EditGreenhouse,
		navigationOptions: {
			title: 'Minhas estufas',
			...stackStyle
		}
	}
})

const app = createBottomTabNavigator({
	Login: {
		screen: UserStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => ( 
				<FontAwesomeIcon name="user-circle"  color={tintColor} size={iconSize} /> 
			)
		}
	},
	Social: {
		screen: SocialStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => ( 
				<FontAwesomeIcon name="comments"  color={tintColor} size={iconSize} /> 
			),
		}
	},
	Plant: {
		screen: PlantStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => ( 
				<FontAwesomeIcon name="book"  color={tintColor} size={iconSize} /> 
				)
			}
		},
	Plantation: {
		screen: PlantationStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => ( 
				<FontAwesomeIcon name="leaf"  color={tintColor} size={iconSize} /> 
			)
		}
	},
	Greenhouse: {
		screen: GreenhouseStack,
		navigationOptions: {
			tabBarIcon: ({tintColor}) => ( 
				<FontAwesomeIcon name="cube"  color={tintColor} size={iconSize} /> 
			)
		}
	}
}, {
	initialRouteName: 'Login',
	order: ['Greenhouse','Plantation','Plant','Social', 'Login'],
	navigationOptions: {
		tabBarVisible: true,
	},
	tabBarOptions: {
		style: {
			borderTopRightRadius: 15,
			borderTopLeftRadius: 15,
			borderWidth: 1,
			borderColor: '#ddd',
		},
		activeTintColor: colors.primary,
		inactiveTintColor: colors.gray,
		showLabel: false,
	},
})

export default createAppContainer(
	createSwitchNavigator({
		SplasScreen: {
			screen: SplasScreen,
			navigationOptions: {
				headerShown: false,
			},
		},
		App: app
	})
)

